<div id="modal" class="modal">
    <header>
        <ul>
            <li id="previous"><button title="Previous Image" onclick="navigation.previous(this);" class="previous"></li>

            <li id="next"><button title="Next Image" onclick="navigation.next(this);" class="next"></button></li>

            <li id="origSize" class="invert"><button title="Original Size" onclick="navigation.origSize(this);" class="img_origSize"></button></li>

            <li id="conWidth"><button title="Fit to Width" onclick="navigation.constrainWidth(this);" class="img_constrainWidth"></button></li>

            <li id="conHeight"><button title="Fit to Height" onclick="navigation.constrainHeight(this);" class="img_constrainHeight"></button></li>

            <li id="closeModal"><button title="Close Window" onclick="navigation.closeModal(this);" class="closeModal"></button></li>
        </ul>
    </header>
    <div><img src='#'></div>
</div>