<section class="index single_header">
<?php
$this->search();
echo "<h2 class='search_results latest'>Latest Stuff</h2>";
echo "<div class='books'>";
echo "<h2 class='light_heading media_heading'>Bookmarks</h2>";
if(!empty($this->book) && count($this->book) > 1){
	echo "<ul>";
	foreach($this->book as $books){ 
		if(isset($books->count)){/**ignore*/}else{?>

	<li>
		<a target="_blank" href="<?=$books->URL?>"><sub>title: </sub><h3 class="light_heading"><?=$books->Title?></h3></a>
		<ul class="tag_entry">
			<sub>tags: </sub>
			<?php 
			$count = 0;
			if(!empty($books->tag_id)){
				$tags = explode(',', $books->tag_id);
				$tag_titles = explode(',', $books->tag_title);
				foreach($tags as $tags){
					echo '<li class="tag_list_item"><a href="'.URL.'bookmarks/index/'.$tags.'" data-id="'.$tags.'">'.$tag_titles[$count].'</a></li>';
					$count++;
				}
			}?>
		</ul>	
	</li>

	<?php } }
	echo "</ul>";
}else{
	$this->no_content('Nothing Found', 'Try adding some <i><b><a href="'.URL.'bookmarks">bookmarks</a></b></i> or <i><b>tagging</b></i> your existing ones');
}
echo "</div>";
echo "<div class='images'>";
echo "<h2 class='light_heading media_heading'>Images</h2>";
if(!empty($this->images) && count($this->images) > 1){
	echo "<ul>";
	foreach($this->images as $images){ 
		if(isset($images->count)){/**ignore*/}else{
			$thumbName = preg_replace('/\\.[^.\\s]{2,5}$/', '', $images->IMGname);;
			$thumbPath = URL.CONTENT_DIR.$images->IMGpath.'thumb/'.$thumbName.'.jpg';?>
	<li>
		<a class="constrain" href="<?=URL.CONTENT_DIR.$images->IMGpath.$images->IMGname?>"><img src="<?=$thumbPath?>" onerror="imgError(this)" width="200px"></a>
		<ul class="tag_entry">
			<sub>tags: </sub>
			<?php 
			$count = 0;
			if(!empty($images->tag_id)){
				$tags = explode(',', $images->tag_id);
				$tag_titles = explode(',', $images->tag_title);
				foreach($tags as $tags){
					echo '<li class="tag_list_item"><a href="'.URL.'images/getImages/1/'.$tags.'" data-id="'.$tags.'">'.$tag_titles[$count].'</a></li>';
					$count++;
				}
			}?>
		</ul>	
	</li>

	<?php } }
	echo "</ul>";
}else{
	$this->no_content('Missing in Action', 'Let\'s see some <b><i><a href="'.URL.'images/">images!</a></i></b> <i>Upload</i> some, <i>tag</i> others or sort through your <b><i><a href="'.URL.'images/temp">Temp Files</a></i></b>');
}
echo "</div>";
echo "<div class='feeds'>";
echo "<h2 class='small_caps_mobile light_heading media_heading'>rss</h2>";
if(!empty($this->feeds) && count($this->feeds) > 1){
	echo "<ul>";
	foreach($this->feeds as $feeds){ 
		if(isset($feeds->count)){/**Ignore*/}else{?>

	<li>
		<sub>title: </sub><h3 class="light_heading"><a target="_blank" href="<?=$feeds->Perma?>"><?=$feeds->Title ? $feeds->Title : 'No title'?></a></h3>
		<ul class="tag_entry">
			<sub>tags: </sub>
			<?php 
			$count = 0;
			if(!empty($feeds->tag_id)){
				$tags = explode(',', $feeds->tag_id);
				$tag_titles = explode(',', $feeds->tag_title);
				foreach($tags as $tags){
					echo '<li class="tag_list_item"><a href="'.URL.'rss/index/'.$tags.'" data-id="'.$tags.'">'.$tag_titles[$count].'</a></li>';
					$count++;
				}
			}?>
		</ul>	
	</li>

	<?php } }
	echo "</ul>";
}else{
	$this->no_content('Notable by it\'s absence', 'Feed the <b><i><a href="'.URL.'rss/">feeds</a><i></b>!');
}
echo "</div>";
?>
</section>