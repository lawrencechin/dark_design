<div id="add_book_form">
	<form onsubmit="form_actions.submitForm(this, event);" method="post" action="<?=URL.'bookmarks/create'?>" name="add_bookmark" class="add_bookmark">
		<h3>Add to Bookmarks</h3>

		<label>
			<span>Bookmark Title</span>
			<input name="book_title" type="text" placeholder="title…" value="">
		</label>

		<input type="hidden" name="book_url" value="">

		<input type="checkbox" name="book_rss" checked style="display: none;">
		
		<label>
		<span>Tags</span>
		<ul class="tag_entry">
			<li class="add_new_tag"><button type="button" title="Add New Tag" onclick="form_actions.addTag(this, '');">+</button></li>
		</ul>
		</label>

		<button title="Submit" class="submit" type="submit"></button>
		<button title="Cancel" onclick="form_actions.removeRssForm(this); return false;" class="cancel"></button>
	</form>
</div>