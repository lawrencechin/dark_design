<section class="bookmarks">
<?php 
	if(!empty($this->urls) && count($this->urls) > 1){
		echo '<ul id="feed_urls" class="vertical_list feed_urls">';

		foreach($this->urls as $urls){ 
			if(isset($urls->count)){/**ignore*/}else{?>
			<li>
				<sub>url: </sub><h3 data-info="<?=$urls->ID?>" data-postURL="<?=URL.'rss/edit/'?>" class="light_heading editable"><?=$urls->URL?></h3>
				<input autocomplete="off" class='edit_name' name='url' type='url' value="<?=$urls->URL?>" placeholder='<?=$urls->URL?>' data-id='<?=$urls->ID?>' data-postURL='<?=URL.'rss/edit/'?>' onblur='form_actions.submitEdit(this)' required>

				<ul class="tag_entry">
					<sub>tags: </sub>
					<?php 
					$count = 0;
					if(!empty($urls->tag_id)){
						$tags = explode(',', $urls->tag_id);
						$tag_titles = explode(',', $urls->tag_title);
						foreach($tags as $tags){
							echo '<li class="tag_list_item"><a href="'.URL.'rss/index/'.$tags.'">'.$tag_titles[$count].'</li></a><a href="'.URL.'rss/removeTag/'.$tags.'/'.$urls->ID.'" class="tag_delete"><span class="removeFav"></span></a>';
							$count++;
						}
					}?>
					<li class="add_new_tag"><button title="Add New Tag" data-id="<?=$urls->ID?>" onclick="form_actions.addTag(this, '<?=URL."rss/addTag"?>');">+</button></li>
				</ul>			

				<div class="button_container">
					<button title="Edit Feed" onclick="form_actions.revealFormInline(this);" class="edit_feed"></button>
					<button title="Delete Feed" onclick="form_actions.deleteFormInline(this);" class="delete_feed"></button>
				</div>

				<form onsubmit="form_actions.submitForm(this, event);" method="post" action="<?=URL.'rss/deleteFeed'?>" class="delete_feed" name="delete_feed">
					<input name="feed_id" type="hidden" value="<?=$urls->ID?>">
					<button title="Delete Feed" type="submit">delete</button>
				</form>
			</li><?php }
		}
	}else{
		$this->no_content('No feeds added', 'Add some feeds using the <i>plus</i> button above. You might like it.');
	}
?>
</section>


