<script>
if($('.img_main').length){
	if(!$('.no_content').length){
		$('#select_all').prop('disabled', false);
		//remove inline tags while editing
		form_actions.removeTag('#image_content');
		navigation.load('#image_content');
	}
}else if($('.temp').length){
	if(!$('.no_content').length){
		temp.tempContent = $('#tempMessageCont');
		temp.selectImages();
	}
}

utils.sidebarFocus();
utils.sidebarEnable();
utils.imagesTweakHeader();
form_actions.form_remove_tag('.secondary_header');
$('.image_tag_list').liveFilter($('#search_tags'), 'li');
</script>

