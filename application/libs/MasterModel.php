<?php
//all models extend from this one allowing us to utilise helper functions used throughout 
class MasterModel{

	public function __construct(Database $db){
		$this->db = $db;
	}

	//helper methods available to all extended models

	public function write_feedback($feedback){
		$_SESSION['feedback_negative'][] = $feedback;
	}

	public function write_positive_feedback($feedback){
		$_SESSION['feedback_positive'][] = $feedback;
	}

	//write to database and return PDO object
	protected function commitDb($sql, $dataArr, $bool, $lastInsertId = false){
		$sth = $this->db->prepare($sql);
		try{
			if($sth->execute($dataArr)){
				//if $bool is set to true then return PDO object to be used in other functions
				if($bool){
					return $sth->fetchAll();
				}
				//if we need the last inserted ID
				if($lastInsertId){
					return $this->db->lastInsertId();
				}
				//successful execute
				return $sth->rowCount();
			}
			//didn't work
			return false;

		}catch(PDOException $e){
			$errorMsg = $e->getMessage();
			return $errorMsg;
		}
	}

	protected function login_params_set($postVar){
		if(!isset($postVar) || empty($postVar)){
			return true;
		}
        return false;
	}

	protected function login_session_write($result){
		//start and write session data 
		Session::init();
        Session::set('user_logged_in', true);
        Session::set('user_id', $result->user_id);
        Session::set('user_name', $result->user_name);
        Session::set('user_email', $result->user_email);
        Session::set('user_style', $result->user_style);
        Session::set('user_edit', $result->user_edit);
	}	 

	//add tags to db
	public function tags($tags, $media_type, $media_id = NULL){
		//grab tag array, loop through and check if tag exists in database(return tag id), tag is new(insert into db), tag is empty(return NULL)
		$user_id = $_SESSION['user_id'];

		for($i = 0; $i < count($tags); $i++){
			if($tags[$i] === ''){
				$tags[$i] = array(NULL, NULL);
			}else{
				$tags[$i] = strtolower(htmlspecialchars($tags[$i]));
				$sql = "SELECT tag_id, tag_title FROM tags WHERE tag_title = :tag_title AND user_id = :user_id";
				$dataArr = array('user_id' => $user_id, 'tag_title' => $tags[$i]);
				$response = $this->commitDb($sql, $dataArr, true);
				//tag exists in database, link tag to media in tag_link_table
				if($response){
					//we don't want to write to tag link table in this instance
					if($media_id === NULL){
						$tags[$i] = array($response[0], NULL);
					}else{
						$sql = "INSERT IGNORE INTO tag_link_table(tag_id, media_type, media_id, user_id) VALUES(:tag_id, :media_type, :media_id, :user_id)";
						$dataArr = array('tag_id' => $response[0]->tag_id, 'media_type' => $media_type, 'media_id' => $media_id, 'user_id' => $user_id);
						$result = $this->commitDb($sql, $dataArr, false);
						$tags[$i] = array($response[0], $result);
					}
				}else{
					//if tag doesn't exist insert into table with count
					$sql = "INSERT INTO tags(user_id, tag_title) VALUES(:user_id, :tag_title)";
					$dataArr = array('user_id' => $user_id, 'tag_title' => $tags[$i]);
					$result = $this->commitDb($sql, $dataArr, false, true);
					$result_two = NULL;

					if($media_id !== NULL){
						$sql = "INSERT IGNORE INTO tag_link_table(tag_id, media_type, media_id, user_id) VALUES(:tag_id, :media_type, :media_id, :user_id)";
						$dataArr = array('tag_id' => $result, 'media_type' => $media_type, 'media_id' => $media_id, 'user_id' => $user_id);
						$result_two = $this->commitDb($sql, $dataArr, false);
					}
					
					$tag_construct = new stdClass;
					$tag_construct->tag_id = $result;
					$tag_construct->tag_title = $tags[$i];
					$tags[$i] = array($tag_construct, $result_two);
				}
			}
		}

		return $tags;
	}

	//function returns tag by section with count
	public function getTags($media_type = NULL, $tag = NULL){
		//NULL media_type returns all tags per user ordered by date created
		if($media_type === NULL){
			$media_join = "";
		}else if($media_type == "i"){
			//return tags for images AND notes
			$media_join = "AND (tlt.media_type = '$media_type' OR tlt.media_type = 'n')";
		}else{
			$media_join = "AND tlt.media_type = '$media_type'";
		}
		$order = $media_type === NULL ? "ORDER BY t.tag_id ASC" : "ORDER BY count DESC"; 
		//media type determines which tags to fetch
		$sql = "SELECT t.tag_id, t.tag_title, Count(t.tag_id) as count 
		FROM tags t
		LEFT JOIN tag_link_table tlt ON tlt.tag_id = t.tag_id
		WHERE t.user_id = :user_id $media_join
		GROUP BY t.tag_id
		$order";

		$newArr = false;
		$this->db->query('SET SQL_BIG_SELECTS=1'); 
		$dataArr = array('user_id' => $_SESSION['user_id']);
		$result = $this->commitDb($sql, $dataArr, true);
		
		foreach($result as $tags){
			$newArr[$tags->tag_id] = array('title' => $tags->tag_title, 'count' => $tags->count);
		}
		if($tag !== NULL && $tag !== 'tl'){
			$tagStr = implode(',', $tag);
			$count = count($tag);
			return $this->relatedTags($media_type, $tagStr, $count, $newArr);
		}else{
			return array(false, false, $newArr);
		}
	}

	//return all tags that include specified tags
	private function relatedTags($media_type, $tagStr, $count, $tagArr){
		if($media_type == 'i'){
			$media_join = "(tlt.media_type = '$media_type' OR tlt.media_type = 'n')";
		}else{
			$media_join = "tlt.media_type = '$media_type'";
		}
		$sql = "SELECT tlt.tag_id, Count(tlt.media_id) as count 
		FROM tag_link_table tlt 
		INNER JOIN (
			SELECT tlt.media_id as tltmid
			FROM tag_link_table tlt
			WHERE tlt.tag_id IN ($tagStr) AND $media_join AND tlt.user_id = :user_id
			GROUP BY tlt.media_id
			HAVING Count(tlt.media_id) = $count) test
			ON tlt.media_id = tltmid 
		WHERE tlt.user_id = :user_id AND $media_join
		GROUP BY tlt.tag_id";

		$dataArr = array('user_id' => $_SESSION['user_id']);
		$result = $this->commitDb($sql, $dataArr, true);
		$tag_search_arr = explode(',', $tagStr);

		foreach($tag_search_arr as $key => $value){
			$testArr[$value] = $value;
		}

		$selectedArr = array();
		$relatedArr = array();

		foreach($result as $test){
			if(array_key_exists($test->tag_id, $testArr)){
				$selectedArr[$test->tag_id] = array('title' => $tagArr[$test->tag_id]['title'], 'id' => $test->tag_id, 'count' => $tagArr[$test->tag_id]['count'], 'related' => false, 'selected' => true, 'relatedCount' => $test->count);
			}else if(array_key_exists($test->tag_id, $tagArr) && !array_key_exists($test->tag_id, $testArr)){
				$relatedArr[$test->tag_id] = array('title' => $tagArr[$test->tag_id]['title'], 'id' => $test->tag_id, 'count' => $tagArr[$test->tag_id]['count'], 'related' => true, 'selected' => false, 'relatedCount' => $test->count);
			}
				
		}
		return array($selectedArr, $relatedArr, $tagArr);
	}

	//seperate function for autocomplete, returns all tags by user
	public function autoTags(){
		$sql = "SELECT tag_id, tag_title FROM tags WHERE user_id = :user_id";
		$dataArr = array('user_id' => $_SESSION['user_id']);
		$result = $this->commitDb($sql, $dataArr, true);

		if($result){
			//return a json response
			$jsonArr = array();
			foreach($result as $tag){
				$jsonArr[] = ['key'=> $tag->tag_id, 'value'=>$tag->tag_title];
			}
			$jArr = json_encode($jsonArr);	
		}else{
			return false;
		}

		return $jArr? $jArr : false;
	}

	public function editTags($tag_id, $tag_name){
		//create sql variables
		//hit up server
		//return resp
		$sql = "UPDATE tags SET tag_title = :tag_title WHERE tag_id = :tag_id AND user_id = :user_id";
		$dataArr = array('tag_id' => $tag_id, 'tag_title' => $tag_name, 'user_id' => $_SESSION['user_id']);
		$result = $this->commitDb($sql, $dataArr, false);
		return $result;
	}

	public function groupEditTags($tag_id = NULL, $media_id, $media_type, $remove){
		$addTags = $tag_id !== NULL? true : false;
		//generate tag ids 
		if($addTags){
			$tags = $this->tags($tag_id, $media_type);
		}
		$user = $_SESSION['user_id'];
		$resultCheck = true;
		//remove selected tags
		if($remove){
			foreach($media_id as $mi){
				//loop through array and remove from tag link table
				foreach($remove as $rm){
					$sql = "DELETE FROM tag_link_table WHERE media_id = :media_id AND user_id = :user_id AND tag_id = :tag_id";
					$dataArr = array('media_id' => $mi, 'user_id' => $user, 'tag_id' => $rm);
					$this->commitDb($sql, $dataArr, false);
				}
			}
		}
		//add new tags
		if($addTags){
			foreach($media_id as $mi){
				//if there are tags to add then do so now looping through tag array
				foreach($tags as $ti){
						$sql = "INSERT IGNORE INTO tag_link_table(media_id, tag_id, user_id, media_type) VALUES(:media_id, :tag_id, :user_id, :media_type)";
						$dataArr = array('media_id' => $mi, 'tag_id' => $ti[0]->tag_id, 'user_id' => $user, 'media_type' => $media_type);
						$resultCheck = $this->commitDb($sql, $dataArr, false);
				}
			}
		}
		
		return true;
	}

	public function removeTag($tag_id, $media_id, $media_type){
		//delete all tags for specified media id
		if($tag_id === NULL){
			$sql = "DELETE FROM tag_link_table WHERE media_id = :media_id AND user_id = :user_id AND media_type = :media_type";
			$dataArr = array('user_id' =>$_SESSION['user_id'], 'media_id' => $media_id, 'media_type' => $media_type);
			$result = $this->commitDb($sql, $dataArr, false);
		//delete specfic tag and media id 	
		}else{
			$sql = "DELETE FROM tag_link_table WHERE tag_id = :tag_id AND media_id = :media_id AND user_id = :user_id AND media_type = :media_type";
			$dataArr = array('tag_id' => $tag_id, 'user_id' =>$_SESSION['user_id'], 'media_id' => $media_id, 'media_type' => $media_type);
			$result = $this->commitDb($sql, $dataArr, false);
		}
		

		return $result;
	}

	public function deleteTags($tag_id){
		//delete tag associations first, in the event of unused tags this won't do anything but we still need to remove the actual tag itself 
		$sql = "DELETE FROM tag_link_table WHERE tag_id = :tag_id AND user_id = :user_id";
		$dataArr = array('tag_id' => $tag_id, 'user_id' =>$_SESSION['user_id']);
		$result = $this->commitDb($sql, $dataArr, false);
		
		//upon successful deletion, remove all tags
		$sql = "DELETE FROM tags WHERE tag_id = :tag_id AND user_id = :user_id";
		$dataArr = array('tag_id' => $tag_id, 'user_id' => $_SESSION['user_id']);
		$result = $this->commitDb($sql, $dataArr, false);
		return $result;
	}

	//single function to return any media in total or filtered by tags
	public function getMedia($media_type, $page = 1, $limit = 0, $tags = NULL){

		$textSearch = NULL;
		//determines the type of search we run
		if(!empty($tags) && $tags !== 'tl'){
			if(is_array($tags)){
				$textSearch = false;
			}else{
				$textSearch = true;
			}
		}
		//run function to populate array with data specific to media type
		if($textSearch === NULL){
			$varArr = $this->setMediaTags($media_type, $page, $limit);
		}else if($textSearch){
			$varArr = $this->setMediaTags($media_type, $page, $limit, $tags);
		}else{
			$varArr = $this->setMediaTags($media_type, $page, $limit);
			//set tag filter if tags have been passed
			$varArr = $this->setFilterTags($tags, $varArr, $media_type);
		}	

		//get media items without tags
		if($tags === 'tl'){
			$sql = "SELECT SQL_CALC_FOUND_ROWS $media_type.ID, tlt.tag_id, $varArr[variables]
			FROM $varArr[table]
			$varArr[join]
			$varArr[where]
			GROUP BY $media_type.ID
			HAVING Count(tlt.tag_id) = 0
			ORDER BY $media_type.ID 
			DESC $varArr[limit] $varArr[page]";
		//or with tags	
		}else{
			//sql query - calc fields to get total count before LIMIT used for pagination
			$sql = "SELECT SQL_CALC_FOUND_ROWS $media_type.ID, $varArr[variables]
			, GROUP_CONCAT(DISTINCT tlt.tag_id SEPARATOR ',') tag_id, 
			GROUP_CONCAT(DISTINCT t.tag_title SEPARATOR ',') tag_title 
			FROM $varArr[table]
			$varArr[extra_join]
			$varArr[search]
			$varArr[join]
			LEFT JOIN tags t ON tlt.tag_id = t.tag_id
			$varArr[where]
			GROUP BY $media_type.ID
			ORDER BY $varArr[order]
			DESC $varArr[limit] $varArr[page]";
		}

		//return $sql;

		$dataArr = array('user_id' => $_SESSION['user_id']);

		$result = $this->commitDb($sql, $dataArr, true);
		//get count for previous sql query and append to $results as count
		$sql = "SELECT FOUND_ROWS() as count";
		$result['count'] = $this->db->query($sql)->fetch();
		return $result;
	}
	//return array with specific variables based on media type
	private function setMediaTags($media_type, $page, $limit, $tags = NULL){
		$varArr = array(
			'variables' => '',
			'table' => '',
			'join' => "LEFT JOIN tag_link_table tlt ON $media_type.ID = tlt.media_id AND tlt.media_type = '$media_type'",
			'extra_join' => '',
			'where' => "WHERE $media_type.user_id = :user_id",
			'page' => $page !== NULL ? "OFFSET " . (($page - 1) * 20) : "",
			'limit' => $limit > 0 ? "LIMIT $limit" : "",
			'search' => '',
			'order' => "$media_type.ID"
			);
		//set variables based on media type
		switch ($media_type){
			case 'i':
			$varArr['variables'] = "i.ID, i.IMGname, i.IMGpath";
			$varArr['table'] = 'Images i';
			$varArr['where'] = $tags === null? $varArr['where'] : $varArr['where'] . " AND tag_title LIKE '%$tags%'";
			break;

			case 'b':
			$varArr['variables'] = "b.ID, b.Title, b.URL";
			$varArr['table'] = 'Bookmarks b';
			$varArr['where'] = $tags === null? $varArr['where'] : $varArr['where'] . " AND b.Title LIKE '%$tags%'";
			break;

			case 'f':
			$varArr['variables'] = "f.ID, f.Title, f.PostDate, f.Description, f.Perma";
			$varArr['table'] = 'Feed_List f';
			$varArr['extra_join'] = "LEFT JOIN Feed_URLS fu ON f.URLID = fu.ID";	
			$varArr['join'] = "LEFT JOIN tag_link_table tlt ON fu.ID = tlt.media_id AND tlt.media_type = 'f' AND tlt.user_id = :user_id";
			$varArr['where'] = $tags === null? $varArr['where'] . " AND f.feed_read = 0" : $varArr['where'] . " AND f.feed_read = 0 AND f.Title LIKE '%$tags%'";
			$varArr['order'] = "f.sortDate";
			break;

			case 'fu' : 
			$varArr['variables'] = "fu.ID, fu.URL";
			$varArr['table'] = 'Feed_URLS fu';
			$varArr['join'] = "LEFT JOIN tag_link_table tlt ON $media_type.ID = tlt.media_id AND tlt.media_type = 'f'";	
			break;

			case 'n':
			$varArr['variables'] = "n.ID, n.content, n.note_colour ";
			$varArr['table'] = 'Notes n';
			break;

			default : 
			return false;
		}

		return $varArr;
	}
	//set up extra tag query when filtering by tags
	private function setFilterTags($tags, $varArr, $media_type){
		$count = count($tags);
		$tagStr = '';
		//loop through tag array to generate string for sql query
		for($i = 0; $i < $count; $i++){
			$tagStr .= $i === 0? $tags[$i] : ', ' . $tags[$i];
		}

		if($media_type === 'f'){
			$varArr['search'] = "INNER JOIN (
			SELECT tlt.media_id
			FROM tag_link_table tlt
			INNER JOIN Feed_URLS fu 
			ON fu.ID = tlt.media_id AND tlt.media_type = '$media_type' AND tlt.user_id = :user_id
			INNER JOIN $varArr[table]
			ON $media_type.URLID = fu.ID AND tlt.user_id = :user_id
			INNER JOIN tags t
			ON t.tag_id = tlt.tag_id AND tlt.user_id = :user_id
			WHERE t.tag_id IN ($tagStr)
			GROUP BY f.ID
			HAVING Count(f.ID) = $count) tltbt
			ON fu.ID = tltbt.media_id";
		}else{
			$varArr['search'] = "INNER JOIN (
			SELECT tlt.media_id
			FROM tag_link_table tlt
			INNER JOIN $varArr[table]
			ON $media_type.ID = tlt.media_id AND tlt.media_type = '$media_type' AND tlt.user_id = :user_id
			INNER JOIN tags t
			ON t.tag_id = tlt.tag_id AND tlt.user_id = :user_id
			WHERE t.tag_id IN ($tagStr)
			GROUP BY tlt.media_id
			HAVING Count(tlt.media_id) = $count) tltbt
			ON $media_type.ID = tltbt.media_id";
		}
		return $varArr;
	}

	public function contentOffline(){
		$this->write_feedback(FEEDBACK_CONTENT_OFFLINE);
	}

	//remove once done
	private function renameFiles($year, $month){
		$dir = new RecursiveIteratorIterator(new RecursiveDirectoryIterator(NON_HTTP_PATH.CONTENT_DIR.$year. '/' .$month));
		return $dir;
	}

	public function exportTags($tags, $dir){
		if(isset($tags) && !empty($tags)){
			$filename = 'tag_list.md';
			$header = "## ".Session::get('user_name')."'s Tag List\n ----\n";
			$tagStr = '';
			foreach($tags[2] as $t){
				$tagStr .= "* $t[title] **count:** $t[count]\n";
			}
			$handle = fopen($dir . $filename, 'w');
            fwrite($handle, $header . $tagStr);
            fclose($handle);
			return array('success' => true, 'path' => $dir, 'filename' => $filename);
		}else{
			return array('success' => false);
		}
	}
}

?>