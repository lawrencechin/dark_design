<div id="loading_and_thumbs" class="loading_and_thumbs">
    <div id="shallNotPass" class="shallNotPass">
        <p>Please Wait</p>
        <div class="progress-button">
            <svg class="progress-circle" width="50" height="50">
                 <path d="M25,2.5 C37.4309405,2.5 47.5,12.5690608 47.5,25 C47.5,37.4309405 37.4309405,47.5 25,47.5 C12.5690595,47.5 2.5,37.4309405 2.5,25 C2.5,12.5690608 12.5690595,2.5 25,2.5 L25,2.5 Z" stroke="#49796B" stroke-width="5" fill="none" stroke-dasharray="1000"></path>
            </svg>

            <svg class="cross" width="50" height="50">
                <path d="M25,25 L0.50000001,0.50000001"></path>
                <path d="M25,25 L49.5,49.5"></path>
                <path d="M25,25 L0.50000001,49.5" id="Shape"></path>
                <path d="M25,25 L49.5,0.50000001" id="Shape"></path>
            </svg>

            <svg class="checkmark" width="50" height="50">
                <path d="M49.5833333,0.652173913 L18.5,49.5000018 L0,35.6521739" stroke-width="3" fill="none"</path>
            </svg>
        </div>
    </div>
</div>