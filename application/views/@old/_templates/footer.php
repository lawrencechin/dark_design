	<footer class="footer">
	<?php $this->renderFooterContent('f'); ?>
	</footer>

    <?php $this->renderFooterContent('m'); ?>
    <?php $this->renderFooterContent('l'); ?>

	<script type="text/javascript" src="//code.jquery.com/jquery-2.1.3.min.js"></script>
	
    <script type="text/javascript" src="<?=URL?>public/js/min/script.min.js"></script>
    <script type="text/javascript" src="<?=URL?>public/js/min/jquery.autocomplete.min.js"></script>
    <script type="text/javascript" src="<?=URL?>public/js/min/mousetrap.min.js"></script>
    <script type="text/javascript" src="<?=URL?>public/js/min/jquery.autosize.input.min.js"></script>
    <script>
        search.tags($('#search', false));
        navigation.homeMenu();
        utils.keyBoardControls();
    </script>

    <?php if($this->checkForActiveController($filename, 'images')){
        $this->footerScripts('images');
    }else if($this->checkForActiveController($filename, 'login')){
        $this->footerScripts('login');
    }else if($this->checkForActiveController($filename, 'rss')){
        $this->footerScripts('rss');
    }else if($this->checkForActiveController($filename, 'bookmarks')){
        $this->footerScripts('bookmarks');
    }else{
       $this->footerScripts('index'); 
    }
    ?>

</body>
</html>