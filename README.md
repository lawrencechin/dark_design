# Dark Design

![home](./readme_images/main.jpg)

A web app for collecting design-related images, bookmarks and rss feeds written in **php**. 

The last version of [Design Stacks](https://gitlab.com/lawrencechin/design_stacks) following on from [Design Stacks Next](https://gitlab.com/lawrencechin/design_stacks_next). The main thrust of this version is a fuller redesign eschewing the sidebar-based design of previous iterations. Dark and light themes have also been added, controlled via an account page that didn't previously exist.

In terms of functionality: the ability to add *markdown-based* notes and an organisational change from albums to tags. In practise, tags are represented in a similar fashion to albums but now media (images, videos, notes) can appear in multiple places via multiple tags. 

## Screenshots

A scant number of screenshots exist for this site, I wonder where they went? Who knows the diverse went of imagery?

![Homepage](./readme_images/homepage.jpg)
![Mobile homepage redesign](./readme_images/homepage_mobile_redesign.jpg)

Mobile homepage redesign from vertical to horizontal layout.

![Image uploads](./readme_images/loading.jpg)

A loading screen when uploading an image. You can, just about, see the new album/tag/category design. The small "*eye*" icon allows you to see an image within the album. Thumbnails were removed from albums due to performance issues (*jank*).

![Categories](./readme_images/categories_2.jpg)

A more fuller look at tags/albums. This is a previous design lacking a few features and shows the dark mode with an incomplete colour scheme. 
