<section class="temp">
<?php
if($this->temp){
	echo '<ul id="temp_files" class="red_content temp_files">';
	foreach($this->temp['files'] as $files){
		if($files['fileAllowable']){
			$imgSrc = URL.LIBS_PATH.'Timthumb.php?src='.urlencode(URL.TEMP_DIR.$files['filePath']).'&w=200';
			$href= '../../'.TEMP_DIR.$files['filePath']; ?>
			<li><a href="<?=$href?>"><img src="<?=$imgSrc?>"></a></li>
		<?php }else{
			$href= '../../'.TEMP_DIR.$files['filePath']; 
			$name = substr($files['filePath'], strpos($files['filePath'], '/')+1);?>
			<li class="non_file"><a href="<?=$href?>"><?=$name?></a></li>
		<?php }
		
	}
	echo '</ul>'; ?>

	<div id="temp_files_viewer" class="temp_files_viewer">
		<div id="tempMessageCont" class="tempMessageCont">
			<div></div>
			<h2>— Nothing selected —</h2>
		</div>
	</div>

<?php }else{
	$this->no_content('Folder is empty', 'Add some files via the <i>add files</i> menu');
}

?>
</section>