<section id="profile" class="profile single_header clearfix">
	<div id="tag_browser" class="tag_browser right left_border">
		<h2>Tag Editor<sub> : double click to edit tag, click off or tab off to finish</h2>
		<input id="search_tags" class="search_tags tag_editor" placeholder="search tags…" type="text">

		<ul id="tagList" class="tag_entry">
			<?php if(!empty($this->tags[0])){
			foreach($this->tags[0] as $key => $value){ ?>

			<li class="tag_list_item" data-id="<?=$key?>">
				<a class="tag_edit" href=#><?=$value?></a>
				<a href="#" class="tag_delete">
					<span class="removeFav"></span>
				</a>
			</li>

			<?php }
				}

			if(!empty($this->tags[1])){
				echo '<h3 class="unused_tags">Unused tags</h3>';
				foreach($this->tags[1] as $key => $value){ ?>
				<li class="tag_list_item" data-id="<?=$key?>">
					<a class="tag_edit" href=#><?=$value?></a>
					<a href="#" class="tag_delete">
						<span class="removeFav"></span>
					</a>
				</li>
				<?php }
			} ?>
		</ul>
	</div>

	<form class="profile_form left" name="edit_user_name" method="post" action="<?=URL?>login/editUsername">
		<h2>Change User Name</h2>
		<label>
		<span>Current User Name</span>
		<input readonly="readonly" name="current_user_name" type="text" value="<?=$_SESSION['user_name']?>" />
		</label>
		<label>
		<span>New User Name</span>
		<input name="user_name" type="text" value="" placeholder="enter new name…" pattern="[a-zA-Z0-9]{2,64}" required />
		</label>
		<button title="Submit" class="submit bottom_border right_border" type="submit"></button>
	</form>

	
	<form class="profile_form left" name="edit_email_address" method="post" action="<?=URL?>login/editUserEmail">
		<h2>Change Email Address</h2>
		<label>
		<span>Current Email Address</span>
		<input readonly="readonly" name="current_email_address" type="text" value="<?=$_SESSION['user_email']?>">
		</label>
		<label>
		<span>New Email Address</span>
		<input name="user_email" type="email" value="" placeholder="enter new email…" required>
		</label>
		<button title="Submit" class="submit bottom_border right_border" type="submit"></button>
	</form>

	<div class="style_select" id="style_select">
		<h2>Select Theme</h2>
		<ul>
			<li class="half_width"><a href="<?=URL.'login/setTheme/dark'?>"><span class="<?=$_SESSION['user_style'] == '0'? 'dark_theme selected_theme' : 'dark_theme'?>"></span><br><span class="theme_text">Dark Theme</span></a></li>
			<li class="half_width"><a href="<?=URL.'login/setTheme/light'?>"><span class="<?=$_SESSION['user_style'] == '1'? 'light_theme selected_theme' : 'light_theme'?>"></span><br><span class="theme_text">Light Theme</span></a></li>
		</ul>
	</div>

	<div class="export_data style_select" id="export_data">
	<h2>Export Data</h2>
	<ul>
		<li class="half_width"><a href="<?=URL.'login/exportBookmarks'?>">Export Bookmarks</a></li>
		<li class="half_width"><a class="green" href="<?=URL.'login/exportFeeds'?>">Export Feeds</a></li>
	</ul>
	</div>
</section>