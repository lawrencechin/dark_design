<section class="register">
    <br>
    <!-- register form -->
    <form method="post" action="<?=URL?>login/register_action" name="registerform">
        <!-- the user name input field uses a HTML5 pattern check -->
        <h2>Register</h2>
        <label>
            <span><sub>Username: </sub>Username(<i>only letters and numbers, 2 to 64 characters</i>)</span>
            <input id="login_input_username" class="login_input" type="text" pattern="[a-zA-Z0-9]{2,64}" name="user_name" required placeholder="enter username…"/>
        </label>
        
        <!-- the email input field uses a HTML5 email type check -->
        <label>
            <span><sub>Email Address: </sub>User's email</span>
            <input id="login_input_email" class="login_input" type="email" name="user_email" required placeholder="enter email…"/>
        </label>
        
        <label>
            <span><sub>Password: </sub>Password (<i>min. 6 characters</i>)</span>
            <input id="login_input_password_new" class="login_input" type="password" name="user_password_new" pattern=".{6,}" required autocomplete="off" placeholder="enter password…"/>
        </label>
        
        <label>
            <span><sub>Password: </sub>Repeat password</span>
            <input id="login_input_password_repeat" class="login_input" type="password" name="user_password_repeat" pattern=".{6,}" required autocomplete="off" placeholder="repeat password…"/>
        </label>
        
        <button type="submit" class="submit right_border bottom_border" name="register"></button>

    </form>
</section>
