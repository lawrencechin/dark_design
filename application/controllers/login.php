<?php

/**
 * Login Controller
 *
**/

class Login extends Controller{
	public function __construct(){
		//note: Controller requires Auth but we need to login to be authorized. If login extends controller then do we need to be logged in to log in?
		parent::__construct();
	}

	public function index(){
		$login_model = $this->loadModel('Login');
		$this->view->render('login/index');
	}

	public function login(){
		$login_model = $this->loadModel('Login');
		//return true or false
		$login_successful = $login_model->login();

		if($login_successful){
			//success! redirect to index/homepage
            Application::app_new_location('index/index');
		}else{
			//boo-hiss, failure. Log in again
            Application::app_new_location('login/index');
		}
	}

    public function demo(){
        $_POST['user_name'] = 'test12User';
        $_POST['user_password'] = 'test12User';
        $login_model = $this->loadModel('Login');
        //return true or false
        $login_successful = $login_model->login();

        if($login_successful){
            //success! redirect to index/homepage
            Application::app_new_location('index/index');
        }else{
            //boo-hiss, failure. Log in again
            Application::app_new_location('login/index');
        }
    }

    public function extLogin(){
        //login to site using extension
        $login_model = $this->loadModel('Login');
        $login_successful = $login_model->login();

        if($login_successful){
            if(isset($_POST['urls']) && !empty($_POST['urls'])){
                $returnVal = $this->getExtImg();
                if($returnVal[0]){
                    if(isset($_SESSION['user_style'])){
                        echo json_encode(array('login' => 1, 'user_style' => Session::get('user_style'), 'error' => false, 'data' => $returnVal[1]));
                        return;
                    }else{
                        echo json_encode(array('login' => 1, 'user_style' => 0, 'error' => false, 'data' => $returnVal[1])); 
                        return;
                    }
                }else{
                    echo json_encode(array('login' => 1, 'user_style' => 0, 'error' => true, 'data' => $returnVal[1]));
                    return;
                }
            }else{
                if(isset($_SESSION['user_style'])){
                    echo json_encode(array('login' => 1, 'user_style' => Session::get('user_style'), 'error' => false));
                    return;
                }else{
                    echo json_encode(array('login' => 1, 'user_style' => 0, 'error' => false)); 
                    return;
                }
            }
        }else{
            echo json_encode(array('login' => 0, 'user_style' => 0));
        }
    }

    public function getExtImg(){ //for use with extension
        //receives urls for imgs
        if(isset($_POST['urls']) && !empty($_POST['urls'])){
            $urls = json_decode($_POST['urls']);
            $login_model = $this->loadModel('Images');
            return $login_model->getExtImgs($urls);
        }else{
            echo json_encode(array('error' => true));
        }
    }

    public function signOut(){
        $login_model = $this->loadModel('Login');
        $login_model->logout();
        echo json_encode(array('logout' => 1));
    }

    public function profile($model = NULL){
        Auth::handleLogin();
        if($model !== NULL){
            $login_model = $model;
        }else{
          $login_model = $this->loadModel('Login');  
        }
        
        $this->view->tags = $login_model->unused_tags();
        $this->view->render('login/profile');
    }

    public function setTheme($selection){
        if(isset($selection)){
            //send integer based on dark or light to model
            $select = $selection == 'dark' ? 0 : 1;
            $login_model = $this->loadModel('Login');
            $result = $login_model->setUserTheme($select);
            
            Application::app_new_location('login/profile');
        }
    }

	public function logout(){
		$login_model = $this->loadModel('Login');
		$login_model->logout();
        Application::app_new_location('');
	}

	public function loginWithCookie(){
		$login_model = $this->loadModel('Login');
		$login_successful = $login_model->loginWithCookie();

		if($login_successful){
            Application::app_new_location('bookmarks/index');
		}else{
			$login_model->deleteCookie();
            Application::app_new_location('login/index');
		}
	}


	public function editUsername(){
        Auth::handleLogin();
		$login_model = $this->loadModel('Login');

        if($this->edit_able){
            $login_model->editUserName();
        }else{
            $login_model->write_feedback('Disabled in demo');
        }
		$this->profile($login_model);
	}

	public function editUserEmail(){
        Auth::handleLogin();
		$login_model = $this->loadModel('Login');
        if($this->edit_able){
            $login_model->editUserEmail();
        }else{
            $login_model->write_feedback('Disabled in demo');
        }
		$this->profile($login_model);
	}

    public function register(){
    	$login_model = $this->loadModel('Login');
    	//$this->view->render('login/register');
        Application::app_new_location('index/index');
    }

    public function register_action(){
    	$login_model = $this->loadModel('Login');
    	$registration_successful = $login_model->registerNewUser();

    	if($registration_successful){
            Application::app_new_location('login/index');
    	}else{
            Application::app_new_location('login/register');
    	}
    }

    public function editTags(){
        if($this->edit_able){
            if(isset($_POST['tag_id']) && !empty($_POST['tag_id']) && $_POST['tag_title'] && !empty($_POST['tag_title'])){
                $login_model = $this->loadModel('Login');
                $result = $login_model->editTags($_POST['tag_id'], $_POST['tag_title']);

                if($result > 0){
                    echo json_encode(array('msg' => 'Tag_Edited', 'error' => false));
                }else{
                    echo json_encode(array('msg' => 'Not working', 'error' => true));
                }
            }
        }else{
            echo json_encode(array('msg' => 'Disabled in demo', 'error' => true));
        }
    }

    public function deleteTags(){
        if($this->edit_able){
            if(isset($_POST['tag_id']) && !empty($_POST['tag_id'])){
                $login_model = $this->loadModel('Login');
                $result = $login_model->deleteTags($_POST['tag_id']);

                if($result > 0){
                    echo json_encode(array('msg' => 'Tag_Deleted', 'error' => false));
                }else{
                    echo json_encode(array('msg' => 'Tag_Undeleted', 'error' => true));
                }
            }
        }else{
            echo json_encode(array('msg' => 'Disabled in demo', 'error' => true));
        }
    }

    public function getTags(){
        $login_model = $this->loadModel('login');
        $tags = $login_model->autoTags();
        echo $tags;
    }

    public function exportBookmarks(){
        $book_model = $this->loadModel('Bookmark');
        $image_model = $this->loadModel('Images');
        //get all bookmarks by user
        $bookmarks = $book_model->getMedia('b', NULL, 0, NULL);
        $dir = $image_model->scanDirectory(true).'/';
        $result = $book_model->exportBookmarks($bookmarks, $dir);
        if($result['success']){
            $path = $result['path'].$result['filename'];
            header('Content-Transfer-Encoding: binary');  // For Gecko browsers mainly
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s', filemtime($path)) . ' GMT');
            header('Accept-Ranges: bytes');  // Allow support for download resume
            header('Content-Length: ' . filesize($path));  // File size
            header('Content-Encoding: none');
            header('Content-Type: text/html');  // Change the mime type if the file is not PDF
            header('Content-Disposition: attachment; filename="' . $result['filename'].'"');  // Make the browser display the Save As dialog
            readfile($path); 
        }else{
            $book_model->write_feedback('No Bookmarks to export');
            $this->profile();
        }
    }

    public function exportFeeds(){
        $rss_model = $this->loadModel('rss');
        $image_model = $this->loadModel('Images');

        $feeds = $rss_model->getMedia('fu', NULL);
        $dir = $image_model->scanDirectory(true).'/';
        $result = $rss_model->exportFeeds($feeds, $dir);

        if($result['success']){
            $path = $result['path'] . $result['filename'];
            header('Content-Transfer-Encoding: binary');  // For Gecko browsers mainly
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s', filemtime($path)) . ' GMT');
            header('Accept-Ranges: bytes');  // Allow support for download resume
            header('Content-Length: ' . filesize($path));  // File size
            //header('Content-Encoding: none');
            header('Content-Type: application/opml');  // Change the mime type if the file is not PDF
            header('Content-Disposition: attachment; filename="' . $result['filename'].'"');  // Make the browser display the Save As dialog
            readfile($path); 
        }else{
            $rss_model->write_feedback('No Feeds to export');
            $this->profile();
        }
    }
    
    public function exportTags(){
        $image_model = $this->loadModel('Images');
        //get all tags by user
        $tags = $image_model->getTags();
        $dir = $image_model->scanDirectory(true).'/';
        $result = $image_model->exportTags($tags, $dir);
        if($result['success']){
            $path = $result['path'].$result['filename'];
            header('Content-Transfer-Encoding: binary');  // For Gecko browsers mainly
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s', filemtime($path)) . ' GMT');
            header('Accept-Ranges: bytes');  // Allow support for download resume
            header('Content-Length: ' . filesize($path));  // File size
            header('Content-Encoding: none');
            header('Content-Type: text/markdown');  // Change the mime type if the file is not PDF
            header('Content-Disposition: attachment; filename="' . $result['filename'].'"');  // Make the browser display the Save As dialog
            readfile($path); 
        }else{
            $book_model->write_feedback('No Tags to export');
            $this->profile();
        }
    }
}






