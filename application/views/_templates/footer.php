	<footer class="footer">
	<?php $this->renderFooterContent('f'); ?>
	</footer>

    <?php $this->renderFooterContent('m'); ?>
    <?php $this->renderFooterContent('l'); ?>
    <div id="bookmark_form" class="persistant bookmark_form">
        <div class="book_note">
            <div class="drag_handle"></div>
            <button id="book_toggle" class="active_toggle" onclick="navigation.toggle_book_note(this)">Add Bookmark</button>
            <button id="note_toggle" onclick="navigation.toggle_book_note(this)">Add Note</button>
        </div>
        <form onsubmit="form_actions.submitForm(this, event);" method="post" action="<?=URL.'bookmarks/create'?>" class="add_bookmark" name="add_bookmark">

            <label>
                <span>Bookmark Title</span>
                <input name="book_title" type="text" placeholder="title…" autocomplete="off">
            </label>

            <label>
                <span>Bookmark URL</span>
                <input name="book_url" type="url" placeholder="url…" autocomplete="off" required>
            </label>

            <label>
                <span>Tags</span>
                <ul class="tag_entry">
                    <li class="add_new_tag"><button type="button" title="Add New Tag" onclick="form_actions.addTag(this, '');">+</button></li>
                </ul>
            </label>

            <button title="Submit" class="submit" type="submit"></button><?php
            ?><button title="Cancel" onclick="form_actions.cancelSubmission(this);" class="cancel"></button>
        </form>
        <form onsubmit='form_actions.submitForm(this, event);' method='post' id='add_note' action="<?=URL.'images/addNote'?>" class="add_note" name='add_note'>

            <label class="note_content">
                <span class="fullWidth">Note Content<a href="<?=URL.'images/getNoteSyntax'?>" id="note_syntax_toggle" class="note_syntax_toggle"><i> (show note syntax)</i></a></span>
                <textarea name='note_content' placeholder='Write…(in markdown or plain text)…' required></textarea>
            </label>

            <div class="note_tags">
            <label>
                <div class="note_colour">
                    <sub>note colour: </sub>
                    <ul>
                        <li><a class="change_colour white_n" href="#" data-colour="note_white"></a></li>
                        <li><a class="change_colour black_n" href="#" data-colour="note_black"></a></li>
                        <li><a class="change_colour green_n" href="#" data-colour="note_green"></a></li>
                        <li><a class="change_colour ruby_n" href="#" data-colour="note_ruby"></a></li>
                        <li><a class="change_colour orange_n" href="#" data-colour="note_orange"></a></li>
                        <li><a class="change_colour blue_n" href="#" data-colour="note_blue"></a></li>
                    </ul>
                </div>
                <span class="fullWidth">Tags</span>
                <ul class="tag_entry">
                    <li class="add_new_tag"><button type="button" title="Add New Tag" onclick="form_actions.addTag(this, '');">+</button></li>
                </ul>
            </label>
            </div>
            
            <button title="Submit" class="submit" type="submit"></button><?php
            ?><button title="Cancel" onclick="form_actions.cancelSubmission(this);" class="cancel"></button>
        </form>
    </div>
    <div id="note_syntax" class="note_syntax"></div>
	<script type="text/javascript" src="//code.jquery.com/jquery-2.2.1.min.js"></script>
    <script type="text/javascript" src="<?=URL?>public/js/min/general.min.js"></script>
    <script type="text/javascript" src="<?=URL?>public/js/min/jquery.autocomplete.min.js"></script>
    <script type="text/javascript" src="<?=URL?>public/js/min/mousetrap.min.js"></script>
    <script>
        search.tags($('#search', false));
        utils.keyBoardControls();
        notes.noteSyntax();
        form_actions.form_remove_tag('#bookmark_form');
        notes.noteColourSelect();
        navigation.imgResize();
    </script>

    <?php if($this->checkForActiveController($filename, 'images')){
        $this->footerScripts('images');
    }else if($this->checkForActiveController($filename, 'login')){
        $this->footerScripts('login');
    }else if($this->checkForActiveController($filename, 'rss')){
        $this->footerScripts('rss');
    }else if($this->checkForActiveController($filename, 'bookmarks')){
        $this->footerScripts('bookmarks');
    }else{
       $this->footerScripts('index'); 
    }
    ?>
</body>
</html>