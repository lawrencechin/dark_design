<?php 

class Images extends Controller{
	public function __construct(){
		parent::__construct();
		Auth::handleLogin();
	}

	public function index(){

		$img_model = $this->loadModel('Images');
		$this->view->tags = $img_model->getTags('i')[2];

		$this->view->mode = "images/indextags";

		$this->view->render('images/indextags');
	}

	public function getFirstImg($tag = NULL){
		$tag = isset($tag) && is_numeric($tag)? array($tag) : $tag; 
		$img_model = $this->loadModel('Images');

		$result = $img_model->getMedia('i', 1, 1, $tag);
		if(!$result){
			echo json_encode(array('img_address' => $result));
		}else{
			$thumbName = preg_replace('/\\.[^.\\s]{2,5}$/', '', $result[0]->IMGname);;
			$thumbPath = URL.CONTENT_DIR.$result[0]->IMGpath.'thumb/'.$thumbName.'.jpg';
			echo json_encode(array('img_address' => $thumbPath));
		}
	}

	public function getImages($pageNumber = 1, $tag = NULL){
		$tagArr = $tag !== NULL && preg_match('~^[0-9]+(,[0-9]+)*$~i', $tag)? explode(',', $tag) : $tag;
		$img_model = $this->loadModel('Images');
		//notes only display on the first page
		if($pageNumber == 1 && $tag !== NULL){
			$this->view->notes = $img_model->getMedia('n', NULL, 0, $tagArr);
		}else{
			$this->view->notes = false;
		}
		//get all tags by media type 
		$this->view->tags = is_array($tagArr) ? $img_model->getTags('i', $tagArr) : $this->view->tags = $img_model->getTags('i');
		//required for searches
		if($tagArr !== NULL && $tagArr !== 'tl' ){
			$this->view->search = str_replace('_', ' ', $tagArr);
		}
		
		//
		$this->view->mode = "images/getImages";

		$this->view->images = $img_model->getMedia('i', $pageNumber, 20, $tagArr);
		if($this->view->images === false && $this->view->notes === false){
			Application::app_new_location('images/index');
		}
		$this->view->pageNumber = $pageNumber;
		$this->view->render('images/index');
	}

	public function getNotes($pageNumber = 1, $tag = NULL){
		$tagArr = $tag !== NULL && preg_match('~^[0-9]+(,[0-9]+)*$~i', $tag)? explode(',', $tag) : $tag;
		$img_model = $this->loadModel('Images');

		$this->view->notes = $img_model->getMedia('n', NULL, 0, $tagArr);
		//get all tags by media type 
		$this->view->tags = is_array($tagArr) ? $img_model->getTags('i', $tagArr) : $this->view->tags = $img_model->getTags('i');
		//required for searches
		if($tagArr !== NULL && $tagArr !== 'tl' ){
			$this->view->search = str_replace('_', ' ', $tagArr);
		}
		//
		$this->view->mode = "images/getImages";

		$this->view->images = NULL;
		if($this->view->notes === FALSE){
			Application::app_new_location('images/index');
		}
		$this->view->pageNumber = $pageNumber;
		$this->view->render('images/index');
	}

	public function addImages(){
		if($this->edit_able){
			$img_model = $this->loadModel('Images');

			if(!is_dir(NON_HTTP_PATH.CONTENT_DIR)){
				$img_model->contentOffline();
				echo json_encode(array('msg'=>'content_offline', 'error'=>true));
				return false;
			}

			if(isset($_FILES['img_files']) 
			&& !empty($_FILES['img_files']) 
			&& isset($_POST['tags'])
			){
				//convert tags into an array
				$tags = !empty($_POST['tags']) ? explode(',', $_POST['tags']) : NULL;
				if(isset($_POST['upload_to_temp'])){
					if($img_model->addImages($_FILES['img_files'], $tags, TRUE)){
						echo json_encode(array('msg'=>'Image_Added', 'error'=>false));
					}	
				}else{
					if($img_model->addImages($_FILES['img_files'], $tags)){
						echo json_encode(array('msg'=>'Image_Added', 'error'=>false));
					}		     
				}
			}
		}else{
			echo json_encode(array('msg'=>'Disabled in demo', 'error'=>true));	
		}
	}

	public function deleteImages(){
		if($this->edit_able){
			$img_model = $this->loadModel('Images');

			if(!is_dir(NON_HTTP_PATH.CONTENT_DIR)){
				$img_model->contentOffline();
				echo json_encode(array('msg'=>'content_offline', 'error'=>true));
				return false;
			}
			//check if either images array is set/not empty OR the temp file array - can only be one or t'other 
			if(isset($_POST['ID']) && !empty($_POST['ID'])){
				$id = $_POST['ID'];
				if(strpos($id, ',') > 0){
					$id = explode(',', $id);
				}else{
					$id = array($id);
				}

				if($img_model->deleteImages(null, $id)){
					echo json_encode(array('msg'=>'Bookmark_Deleted', 'error'=>false));
				}
			}else if(isset($_POST['imgArr']) && !empty($_POST['imgArr'])){
				$imgArr = explode(',' , $_POST['imgArr'][0]);
				//second param will be populated by model method
				if($img_model->deleteImages($imgArr)){
					echo json_encode(array('msg'=>'Temp_Image_Deleted', 'error'=>false));
				}
			}
		}else{
			echo json_encode(array('msg'=>'Disabled in demo', 'error'=>true));
		}
	}

	public function setMediaType(){
		return 'i';
	}	

	public function temp(){
		$img_model = $this->loadModel('Images');

		if(!is_dir(NON_HTTP_PATH.CONTENT_DIR)){
			$img_model->contentOffline();
			Application::app_new_location('images/index');
		}

		$this->view->temp = $img_model->scanDirectory();
		$this->view->render('images/temp');
	}

	public function moveTempFiles(){
		if($this->edit_able){
			$img_model = $this->loadModel('Images');

			if(!is_dir(NON_HTTP_PATH.CONTENT_DIR)){
				$img_model->contentOffline();
				echo json_encode(array('msg'=>'content_offline', 'error'=>true));
				return false;
			}

			if(isset($_POST['imgArr']) && !empty($_POST['imgArr'])&& isset($_POST['tags'])){

				$tags = !empty($_POST['tags']) ? explode(',', $_POST['tags']) : NULL;
				$imgArr = explode(',' , $_POST['imgArr'][0]);

				if($img_model->moveTempFiles($imgArr, $tags)){
					echo json_encode(array('msg'=>'Temp_Image_Deleted', 'error'=>false));
				}
			}
		}else{
			echo json_encode(array('msg'=>'Disabled in demo', 'error'=>true));
		}
	}

	public function addNote(){
		if($this->edit_able){
			if(isset($_POST['note_content']) && !empty($_POST['note_content']) && isset($_POST['tags']) && isset($_POST['note_colour']) && !empty($_POST['note_colour'])){

				$colour_arr = ['note_white', 'note_black', 'note_orange', 'note_ruby', 'note_blue', 'note_green'];
				//check if posted colour is valid otherwise default to black
				$colour = $_POST['note_colour'];
				if(!in_array($_POST['note_colour'], $colour_arr)){
					$colour = 'note_black';
				}

				$content = strip_tags(htmlspecialchars($_POST['note_content']));
				$tags = !empty($_POST['tags']) ? explode(',', $_POST['tags']) : NULL;
				$img_model = $this->loadModel('Images');
				$result = $img_model->addNote($content, $tags, $colour);
				if($result){
					echo json_encode(array('msg' => 'note_added', 'error' => false));
				}else{
					echo json_encode(array('msg' => 'note_not_added', 'error' => true));
				}
			}else{
				echo json_encode(array('msg' => 'note_not_added', 'error' => true));
			}
		}else{
			echo json_encode(array('msg' => 'Disabled in demo', 'error' => true));
		}
	}

	public function editNote(){
		if($this->edit_able){
			if(isset($_POST['note_id']) && !empty($_POST['note_id']) && isset($_POST['note_content']) && !empty($_POST['note_content']) && isset($_POST['note_colour']) && !empty($_POST['note_colour'])){

				$colour_arr = ['note_white', 'note_black', 'note_orange', 'note_ruby', 'note_blue', 'note_green'];
				//check if posted colour is valid otherwise default to black
				$colour = $_POST['note_colour'];
				if(!in_array($colour, $colour_arr)){
					$colour = 'note_black';
				}

				$content = strip_tags(htmlspecialchars($_POST['note_content']));
				$img_model = $this->loadModel('Images');

				$result = $img_model->editNote($_POST['note_id'], $content, $colour);

				if($result > 0){
					$Parsedown = new Parsedown();
					echo json_encode(array('msg' => 'note_updated', 'error' => false, 'content' => $Parsedown->text($content)));
				}else{
					echo json_encode(array('msg' => 'note_not_updated', 'error' => false));
				}
			}else{
				echo json_encode(array('msg' => 'note_not_updated', 'error' => false));
			}
		}else{
			echo json_encode(array('msg' => 'note_not_updated', 'error' => false));
		}
	}

	public function deleteNote(){
		if($this->edit_able){
			if(isset($_POST['note_id']) && !empty($_POST['note_id'])){
				$img_model = $this->loadModel('Images');
				$result = $img_model->deleteNote($_POST['note_id']);

				if($result){
					echo json_encode(array('msg' => 'note_deleted', 'error' => false));
				}else{
					echo json_encode(array('msg' => 'note_not_deleted', 'error' => true));
				}
			}else{
				echo json_encode(array('msg' => 'note_not_deleted', 'error' => true));
			}
		}else{
			echo json_encode(array('msg' => 'note_deleted', 'error' => false));
		}
	}

	public function getNoteContent($id){
		if(isset($id) && !empty($id)){
			$img_model = $this->loadModel('images');
			$result = $img_model->getNoteContent($id);
			echo $result;
		}else{
			echo 'no data';
		}
	}

	public function getNoteSyntax(){
		$this->view->render('images/noteSyntax', true);
	}
}