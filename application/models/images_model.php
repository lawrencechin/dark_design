<?php 
class ImagesModel extends MasterModel{
	public function __construct(Database $db){
		parent::__construct($db);
	}

	private function nameGenerator($file, $fileExt){
		$hash = hash_file('sha1', $file);
		$date = date("Y/m/").substr($hash, 0, 2).'/';
		return array('location' => $date, 'thumb_location' => $date.'thumb/'.$hash.".jpg", 'file_name' => $hash.".".$fileExt);
	}

	private function createDir($location){
		if(!is_dir(NON_HTTP_PATH.CONTENT_DIR.$location) && !is_dir(NON_HTTP_PATH.CONTENT_DIR.$location."thumb/")){
			$oldmask = umask(0);
			mkdir(NON_HTTP_PATH.CONTENT_DIR.$location, 0777, true);
			mkdir(NON_HTTP_PATH.CONTENT_DIR.$location."thumb/", 0777, true);
			umask($oldmask);
		}
	}

	private function thumbnail($in, $out){
	    $image = WideImage::load($in)->resize(THUMB_WIDTH, THUMB_HEIGHT, 'outside')->crop('center', 'center', THUMB_WIDTH, THUMB_HEIGHT)->saveToFile($out);
	}

	private function resizeImage($in, $out){
		$dimensions = getImagesize($in);
		$width = $dimensions[0];
		$height = $dimensions[1];
		if($width <= 0 || $height <= 0){
			return array('error' => false, 'msg' => 'not an image');
		}
		$wider = $width > $height ? true : false;
		$ratio = $wider ? $width/$height : $height/$width;
		if($width > 3000 || $height > 3000){ //resize image if too large
			$newWidth = $wider ? 3000 : round(3000/$ratio, 0);
			$newHeight = $wider ? round(3000/$ratio, 0) : 3000;
			try{
				$image = WideImage::load($in)->resize($newWidth, $newHeight, 'outside')->saveToFile($out);
				return array('error' => false, 'msg' => 'resized');
			}catch(Exception $e){
				return array('error' => true, 'msg' => $e->getMessage());
			}
		}else{
			return array('error' => false, 'msg' => 'not_resized');
		}
	}

	private function imageCheck($newFile = array()){
		$sql = "SELECT IMGname, IMGpath, user_id FROM Images WHERE 
		IMGname = :IMGname";
		$dataArr = array(
			'IMGname' => $newFile['IMGname']
			);
		$result = $this->commitDb($sql, $dataArr, true);
		//check if file exists and belongs to current user
		if($result){
			if($result[0]->user_id == $_SESSION['user_id']){
				//no need to move files or write to db, it already exists
				return array('img_exists' => true, 'img_belongs_current_user' => true, 'file_details' => $result);
			}
			//someone else has uploaded the file, we can write to db but don't move the photo
			return array('img_exists' => true, 'img_belongs_current_user' => false, 'file_details' => $result);
		}
		return array('img_exists' => false, 'img_belongs_current_user' => false, 'file_details' => false);
	}

	private function moveFiles($file, $fileExt){
		$imageFileTypes = array('jpeg', 'jpg', 'gif', 'png', 'tiff');
		//generate names and paths
		$newName = $this->nameGenerator($file['tmp_name'], $fileExt);

		$newFile = array('result' => true, 'IMGname' => $newName['file_name'], 'IMGpath' => $newName['location'], 'writedb' => true);

		//check if files already exist in database 
		$imageCheck = $this->imageCheck($newFile);

		if(!$imageCheck['img_exists']){
			//mkdirs(or not if they already exist)
			$this->createDir($newName['location']);
			//move uploaded files to new location
			if(move_uploaded_file($file['tmp_name'], NON_HTTP_PATH.CONTENT_DIR.$newName['location'].$newName['file_name'])){
				//resize image, return response
				$resize = $this->resizeImage(NON_HTTP_PATH.CONTENT_DIR.$newName['location'].$newName['file_name'], NON_HTTP_PATH.CONTENT_DIR.$newName['location'].$newName['file_name']);

				if(!$resize['error']){
					$in = NON_HTTP_PATH.CONTENT_DIR.$newName['location'].$newName['file_name'];
					$out = NON_HTTP_PATH.CONTENT_DIR.$newName['thumb_location'];
					$this->thumbnail($in, $out);
				}
				//success! return new name(for other functions) and true
				return $newFile;
			}
		}else if($imageCheck['img_exists'] && !$imageCheck['img_belongs_current_user'] || $imageCheck['img_exists'] && $imageCheck['img_belongs_current_user']){
			//we want to write to db but not move file - also we need the old path rather than the newly generated one
			$newFile['IMGpath'] = $imageCheck['file_details'][0]->IMGpath;
			return $newFile;
		}
		
		//failure
		return array('result' => false);
	}

	private function writeFilesDb($fileArr, $tags = NULL){
		$sql = "INSERT INTO Images(user_id, IMGname, IMGpath) VALUES(:user_id, :IMGname, :IMGpath)";
		$dataArr = array(
			'user_id' => $_SESSION['user_id'],
			'IMGname' => $fileArr['IMGname'],
			'IMGpath' => $fileArr['IMGpath']
			);
		//return ID of inserted image
		$result = $this->commitDb($sql, $dataArr, false, true);
		if($result){
			//write tags
			if($tags !== NULL){
				$this->tags($tags, 'i', $result);
				return true;
			}else{
				return true;
			}	
		}
		return false;
	}

	private function fileErrors($fileErr = array(), $fileArrCount, $feedback){
		//check if any errors were collected
		if(count($fileErr) > 0){
			//check if ALL files resulted in error
			if(count($fileErr) === $fileArrCount){
				$this->write_feedback($feedback.implode(', ', $fileErr));
				return false;
			}
			//return true if SOME files were errors
			$this->write_feedback($feedback.implode(', ', $fileErr));
			return true;
		}
	}

	public function addImages($uploads, $tags, $temp = FALSE){
		$imageFileTypes = array('jpeg', 'jpg', 'gif', 'png', 'tiff');
		if(isset($uploads) && count($uploads) > 0){
			//rearrange file array to make using it easier
			foreach($uploads as $key=>$value){
		    	foreach($value as $key2=>$value2){
		    		$fileArr[$key2][$key] = $value2;
		    	}
		    }
		    //collect files that could not be uploaded
		    $errorFileArr = array();

		    for($i = 0; $i < count($fileArr); $i++){
		    	//get file extension to check against array
		    	$fileInfo = pathinfo($fileArr[$i]['name']);
		    	$fileExt = strtolower($fileInfo['extension']);
		    	//if we want to move to temp
		    	if($temp){
		    		//generate new name
		    		$newName = $this->nameGenerator($fileArr[$i]['tmp_name'], $fileExt);
		    		//move files to directory
		    		if(in_array($fileExt, $imageFileTypes)){

		    			$dir = $this->tempFolder()[0]->user_creation_timestamp;
						$segment_1 = substr($dir, 0, 4);
						$segment_2 = substr($dir, 4);
						$id = $_SESSION['user_id'];
						$siteSeg = "darkdesign";
						$final = $siteSeg . '-' .$segment_1 . $id . $segment_2;

		    			if(move_uploaded_file($fileArr[$i]['tmp_name'], NON_HTTP_PATH.TEMP_DIR.$final.'/'.$newName['file_name'])){
		    				$resize = $this->resizeImage(NON_HTTP_PATH.TEMP_DIR.$final.'/'.$newName['file_name'], NON_HTTP_PATH.TEMP_DIR.$final.'/'.$newName['file_name']);
		    				if($resize['error']){
		    					$errorFileArr[] = $newName['file_name'];
		    				}
		    			}else{
		    				$errorFileArr[] = $fileArr[$i]['tmp_name'];
		    			}
		    		}
		    	}else{
		    		//if in image type array
			    	if(in_array($fileExt, $imageFileTypes)){
			    		//move files
			    		$result = $this->moveFiles($fileArr[$i], $fileExt);
			    		//if success
			    		if($result['result']){
			    			if($result['writedb']){
			    				//write to db
			    				$newResult = $this->writeFilesDb($result, $tags);
				    			if($newResult){//ok - continue
				    			}else{
				    				//collect problem files
				    				$errorFileArr[] = $fileArr[$i]['name'];
				    			}	
			    			}
			    			
			    		}else{
			    			//collect problems files
			    			$errorFileArr[] = $fileArr[$i]['name'];
			    		}
			    	}else{
			    		//collect problem files
			    		$errorFileArr[] = $fileArr[$i]['name'];
			    	}
		    	}   	
		    }

		    $this->fileErrors($errorFileArr, count($fileArr), FEEDBACK_IMAGES_UPLOAD_ERROR);
		    return true;		    
		}
		$this->write_feedback(FEEDBACK_GET_IMAGE_ERROR);
		return false;
	}
	
	public function deleteImages($fileArr = NULL, $fileIDArr = NULL){
		//delete function accounts for temp files and database files. Only one array will be set at time of function call
		$removeErr = array();
		//temp files!
		if(count($fileArr) > 0){
			for($i = 0; $i < count($fileArr); $i++){
				//note that the substr amount changes depending on a top level domain or a sub folder
				$removePath = NON_HTTP_PATH.substr($fileArr[$i], 5);
				//check if file exists
				if(!file_exists($removePath)){
					//if it does not add to error array
					$removeErr[] = $fileArr[$i];
				}else{
					//try to unlink, failing that add to error array
					if(unlink($removePath)){
					}else{
						$removeErr[] = $fileArr[$i];
					}
				}
			}
			$this->fileErrors($removeErr, count($fileArr), FEEDBACK_FILES_REMOVE_FAIL);
			return true;
		}//db call
		else if(count($fileIDArr) > 0){
			for($i = 0; $i < count($fileIDArr); $i++){
				$sql = "SELECT ID, IMGname, IMGpath FROM Images WHERE ID = :ID AND user_id = :user_id";
				$dataArr = array(
					'user_id' => $_SESSION['user_id'],
					'ID' => $fileIDArr[$i]
					);
				$result = $this->commitDb($sql, $dataArr, true);

				if($result){
					$sql = "SELECT Count(*) AS count FROM Images WHERE IMGname = :IMGname";
					$dataArr = array('IMGname' => $result[0]->IMGname);
					$imgCount = $this->commitDb($sql, $dataArr, true);

					//check if there are multiple entries for the image, if so remove from DB but DON'T delete file!!!!!!
					if((int)$imgCount[0]->count === 1){
						$img = NON_HTTP_PATH.CONTENT_DIR.$result[0]->IMGpath.$result[0]->IMGname;
						$thumbName = preg_replace('/\\.[^.\\s]{2,5}$/', '', $result[0]->IMGname);;
						$thumbPath = URL.CONTENT_DIR.$result[0]->IMGpath.'thumb/'.$thumbName.'.jpg';
						$thumb = NON_HTTP_PATH.CONTENT_DIR.$result[0]->IMGpath.'thumb/'.$thumbName.'.jpg';

						if(is_file($img)){
							unlink($img);
							unlink($thumb);
							//remove folders if empty
							$isDirEmpty = !(new \FilesystemIterator(NON_HTTP_PATH.CONTENT_DIR.$result[0]->IMGpath."thumb"))->valid();
							if($isDirEmpty == 1){
								if(is_file(NON_HTTP_PATH.CONTENT_DIR.$result[0]->IMGpath.'.DS_Store')){
									unlink(URL.CONTENT_DIR.$result[0]->IMGpath.'.DS_Store');
								}
								rmdir(NON_HTTP_PATH.CONTENT_DIR.$result[0]->IMGpath."thumb");
								rmdir(NON_HTTP_PATH.CONTENT_DIR.$result[0]->IMGpath);
							}
						}
					}	
				}else{
					$removeErr[] = $fileIDArr[$i];
				}

				$sql = "DELETE FROM Images WHERE user_id = :user_id AND ID = :ID";
				$dataArr = array(
					'user_id' => $_SESSION['user_id'],
					'ID' => $result[0]->ID
					);
				//we still need the previous $result for tag removal
				$result_del = $this->commitDb($sql, $dataArr, false);

				if((int)$result_del === 0){
					$removeErr[] = $fileIDArr[$i];
				}else{
					//remove tags associations
					$this->removeTag(NULL, $result[0]->ID, 'i');
				}
			}

			$this->fileErrors($removeErr, count($fileIDArr), FEEDBACK_FILES_REMOVE_FAIL);
			return true;
		}
	}

	private function tempFolder(){
		$sql = "SELECT user_creation_timestamp FROM users WHERE user_id = :user_id";
		$dataArr = array('user_id' => $_SESSION['user_id']);
		$result = $this->commitDb($sql, $dataArr, true);

		return $result;
	}

	//temp files functions
	public function scanDirectory($getDir = false){
		//arrays of data to compare against files or folders
		$displayThumbArr = ['jpg', 'jpeg', 'gif', 'bmp', 'png', 'svg', 'tiff', 'webp'];
		$forbiddenArray = array('.DS_Store', urldecode('Icon%0D'));
		$data = array();

		$dir = $this->tempFolder()[0]->user_creation_timestamp;
		$segment_1 = substr($dir, 0, 4);
		$segment_2 = substr($dir, 4);
		$id = $_SESSION['user_id'];
		$siteSeg = "darkdesign";
		$final = $siteSeg . '-' .$segment_1 . $id . $segment_2;

		if(!is_dir(NON_HTTP_PATH.TEMP_DIR.$final)){
			$oldmask = umask(0);
			mkdir(NON_HTTP_PATH.TEMP_DIR.$final, 0777, true);
			umask($oldmask);
		}

		$directory = NON_HTTP_PATH.TEMP_DIR.$final;
		if($getDir){return $directory;}

		//scan directory and grab data, put into data array
		foreach(new DirectoryIterator($directory) as $file_or_folder){
			if($file_or_folder->isFile()){
				$fileExt = strtolower($file_or_folder->getExtension());
				if(!in_array($file_or_folder->getBasename(), $forbiddenArray)){
					if(in_array($fileExt, $displayThumbArr)){
						$data['files'][] = array('filePath' => $filePath = $final.'/'.$file_or_folder->getFilename(), 'fileExtension' => $fileExt, 'fileAllowable' => true);
					}else{
						$data['files'][] = array('filePath' => $filePath = $filePath = $final.'/'.$file_or_folder->getFilename(), 'fileExtension' => $fileExt, 'fileAllowable' => false);
					}
				}
			}
		}
		//return obj 
		return $data;
	}

	public function moveTempFiles($fileArr = array(), $tags = NULL){
		$removeErr = array();
		$imageFileTypes = array('jpeg', 'jpg', 'gif', 'png', 'tiff');

		if(count($fileArr) > 0){
			for($i = 0; $i < count($fileArr); $i++){
				$fileExt = pathinfo($fileArr[$i], PATHINFO_EXTENSION);
				if(in_array($fileExt, $imageFileTypes)){
					//generate names and paths into array
					//note that the substr amount changes depending on a top level domain or a sub folder
					$newName = $this->nameGenerator(NON_HTTP_PATH.substr($fileArr[$i], 5), $fileExt);
					$fullPathIn = NON_HTTP_PATH.substr($fileArr[$i], 5);
					$fullPathOut = NON_HTTP_PATH.CONTENT_DIR.$newName['location'].$newName['file_name'];

					//before moving or writing to db - run imageCheck
					$imageCheck = $this->imageCheck(array('IMGname' => $newName['file_name']));

					if($imageCheck['img_exists']){
						//remove input as file is already in place
						unlink($fullPathIn);
						//change the new location with the old
						$newName['location'] = $imageCheck['file_details'][0]->IMGpath;
					}else{
						//mkdirs
						$this->createDir($newName['location']);
						//move files
						if(copy($fullPathIn, $fullPathOut)){
							unlink($fullPathIn);
							$in = NON_HTTP_PATH.CONTENT_DIR.$newName['location'].$newName['file_name'];
							$out = NON_HTTP_PATH.CONTENT_DIR.$newName['thumb_location'];
							$this->thumbnail($in, $out);
						}
					}

					$newFile = array(
						'IMGname' => $newName['file_name'],
						'IMGpath' => $newName['location']
						);

					$this->writeFilesDb($newFile, $tags);

				}else{
					$removeErr[] = pathinfo($fileArr[$i], PATHINFO_FILENAME);
				}
			}

			$this->fileErrors($removeErr, count($fileArr), FEEDBACK_FILES_MOVE_FAIL);
			return true;
		}
	}

	//notes

	public function addNote($content, $tags = NULL, $noteColour){
		$sql = "INSERT INTO Notes(content, note_colour, user_id) VALUES (:content, :note_colour, :user_id)";
		$dataArr = array(
			'content' => $content,
			'note_colour' => $noteColour,
			'user_id' => $_SESSION['user_id']
			);
		$result = $this->commitDb($sql, $dataArr, false, true);

		if($result && $tags !== NULL){
			$this->tags($tags, 'n', $result);
		}
		return $result;
	}

	public function editNote($id, $content, $noteColour){
		$sql = "UPDATE Notes SET content = :content, note_colour = :note_colour WHERE ID = :ID AND user_id = :user_id";
		$dataArr = array(
			'ID' => $id, 
			'content' => $content,
			'note_colour' => $noteColour,
			'user_id' => $_SESSION['user_id']
			);
		return $result = $this->commitDb($sql, $dataArr, false);
	}

	public function deleteNote($id){
		$sql = "DELETE FROM Notes WHERE id = :id AND user_id = :user_id";
		$dataArr = array('id' => $id, 'user_id' => $_SESSION['user_id']);
		$result = $this->commitDb($sql, $dataArr, false);
		if($result > 0){
			$this->removeTag(NULL, $id, 'n');
			return $result;
		};
	}

	public function getNoteContent($id){
		$sql = "SELECT content FROM Notes WHERE id = :id AND user_id = :user_id";
		$dataArr = array('id' => $id, 'user_id' => $_SESSION['user_id']);
		$result = $this->commitDb($sql, $dataArr, true);

		if($result){
			return $result[0]->content;
		}else{
			return false;
		}
	}

	public function getExtImgs($imgArr = NULL){
		//check for or create user temp folder
		if($imgArr !== NULL){
			$dir = $this->tempFolder()[0]->user_creation_timestamp;
			$segment_1 = substr($dir, 0, 4);
			$segment_2 = substr($dir, 4);
			$id = $_SESSION['user_id'];
			$siteSeg = "darkdesign";
			$final = $siteSeg . '-' . $segment_1 . $id . $segment_2;

			if(!is_dir(NON_HTTP_PATH.TEMP_DIR.$final)){
				$oldmask = umask(0);
				mkdir(NON_HTTP_PATH.TEMP_DIR.$final, 0777, true);
				umask($oldmask);
			}

			$directory = NON_HTTP_PATH.TEMP_DIR.$final.'/';
			$errorLog = array();
			foreach($imgArr as $url){
				//check files are actual links, download images using cURL
				if(!empty($url) && strpos($url, 'http') === 0){
					$lastPeriod = strrpos($url, '.');
					$extension = substr($url, $lastPeriod);
					//remove url params 
					$cleanupExt = strpos($extension, '&');
					if($cleanupExt > 0){
						$extension = substr($extension, 0, $cleanupExt);
					}
					//remove further stuff from extension
					$cleanupExt = strpos($extension, '?');
					if($cleanupExt > 0){
						$extension = substr($extension, 0, $cleanupExt);
					}
					//generate timestamp for each image - used as filename
					$dateName = Date('U') . '-' . rand(0, 100);
					//open new file handle for image
					$file_handler = fopen($directory.$dateName.'.'.$extension, 'w+');
					$curl = curl_init($url); //start curl
					$options = array(
						CURLOPT_FILE => $file_handler,
						//CURLOPT_HEADER => 0,
						CURLOPT_FOLLOWLOCATION => 1,
						CURLOPT_TIMEOUT => 1000000,
						//CURLOPT_USERAGENT => 'Mozilla/5.0',
						CURLOPT_SSL_VERIFYPEER=> 0
					);
					curl_setopt_array($curl, $options);
					curl_exec($curl);
					if(curl_errno($curl)){ //check for errors
						array_push($errorLog, [
							'url' => $url,
							'error' => true,
							'msg' => curl_error($curl)
							]);
					}
					curl_close($curl);
					fclose($file_handler);
					if(file_exists($directory.$dateName.'.'.$extension)){ //resize if successful
						$this->resizeImage($directory.$dateName.'.'.$extension, $directory.$dateName.'.'.$extension);
					}
				}	
			}
			if(count($errorLog) > 0){
				return array(false, $errorLog);
			}else{
				return array(true, false);
			}
		}else{
			return array(true);
		}
	}
}

?>