module.exports = function(grunt){
	'use strict';
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		uglify: {
			options: {
				mangle: false,
				preserveComments: false,
				compress : true
			},
				
			compile: {
				files: {
			        'public/js/min/script.min.js': ['public/js/fragments/*.js']
			      }
			}
		},

		cssmin: {
			dark : {
				options: {
			    	shorthandCompacting: false,
			    	roundingPrecision: -1
				},
			    files: {
			       'public/css/screen-dark.min.css' : 'public/css/screen-dark-prefixed.css',
			    }
			},

			light : {
				options: {
			    	shorthandCompacting: false,
			    	roundingPrecision: -1
				},
				
			    files: {
			       'public/css/screen-light.min.css' : 'public/css/screen-light-prefixed.css',
			    }
			},
			
		},

		sass: {
			dark: {
				options: {
					style : 'compact',
					trace : true
				},

				files:[{
					expand: true,
					cwd: 'public/css/dark/sass/',
					src: '*.scss',
					dest: 'public/css/dark',
					ext: '.css'
				}]
			},
			light: {
				options: {
					style : 'compact',
					trace : true
				},

				files:[{
					expand: true,
					cwd: 'public/css/light/sass/',
					src: '*.scss',
					dest: 'public/css/light',
					ext: '.css'
				}]
			}
		},

		autoprefixer: {
			dark: {
				files: {
					'public/css/screen-dark-prefixed.css' : 'public/css/dark/screen-dark.css'
				}
			},
			light: {
				files: {
					'public/css/screen-light-prefixed.css' : 'public/css/light/screen-light.css'
				}
			}
		},

		watch: {
            dark: {
                files: ['public/css/dark/sass/*.scss'],
                tasks: ['sass', 'autoprefixer', 'cssmin']
            },

            light: {
            	files: ['public/css/light/sass/*.scss'],
                tasks: ['sass', 'autoprefixer', 'cssmin']
            },

            uglify: {
            	files: ['public/js/fragments/*.js'],
            	tasks: ['uglify']
            },

            options: {
            	spawn : false,
            	event : 'changed'
            }
        }
	});

	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-sass');
	grunt.loadNpmTasks('grunt-autoprefixer');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	//Default task(s)
	grunt.registerTask('compile', 'uglify');
};