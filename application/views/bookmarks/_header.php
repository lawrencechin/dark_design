<nav class="secondary_header">
	<ul>
		<li onclick="utils.sidebarRevealMobile();" class="no_padding"><button disabled title="Reveal Sidebar" class="sidebarReveal" id="sidebarReveal"></button></li><?php
		?><li class="no_padding"><button title="Add Bookmark" class="add_bookmark" onclick="form_actions.revealForm(this)"></button></li><?php
		?><li class="no_padding"><button title="Edit Multiple Tags" disabled="disabled" onclick="form_actions.revealForm(this);" class="edit_multi"></button></li><?php
		?><li class="no_padding"><button title="Delete Selected Bookmarks" disabled="disabled" onclick="form_actions.bookmark_delete_multi(this);" class="delete_temp_files"></button></li><?php
		?><li class="no_padding"><button title="Select All/Deselect Selected" disabled="disabled" onclick="form_actions.checkAll(this, '#bookmarks_list');" id="select_all" class="check_image no_margin"></button></li>
	</ul>

	<form onsubmit="form_actions.submitForm(this, event);" method="post" action="<?=URL.'bookmarks/editMulti'?>" class="edit_multi" name="edit_multi" id="edit_multi">
		<h3>Edit Multiple Tags <sub>(for selected images)</sub></h3>
		<label>
			<span>Remove current tags? <sub>Check box to remove current tags. Highlighted tags will be removed.</sub></span>
				<input type="checkbox" name="remove_tags" class="remove_tags" onclick="form_actions.tag_toggle(this)">
				<ul class="tag_entry tag_removal"></ul>
		</label>

		<label>
			<span>Tags</span>
			<ul class="tag_entry">
				<li class="add_new_tag"><button type="button" title="Add New Tag" onclick="form_actions.addTag(this, '');">+</button></li>
			</ul>
		</label>

		<button title="Submit" class="submit" type="submit"></button><?php
		?><button type="button" title="Cancel" onclick="form_actions.cancelSubmission(this); return false;" class="cancel"></button>
	</form>
</nav>