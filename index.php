<?php

//load config file(error reporting, database, paths)
require 'application/config/config.php';
require 'application/config/feedback.php';
require 'application/config/autoload.php';

if(file_exists('vendor/autoload.php')){
	require 'vendor/autoload.php';
}

//Start your engines

$app = new Application;


