<?php

// get the feedback (they are arrays, to make multiple positive/negative messages possible)
$feedback_positive = Session::get('feedback_positive');
$feedback_negative = Session::get('feedback_negative');
$feedback_template_pre = '<div class="feedback success">';
$feedback_template_post = '</p></div>';
$feedback_template_close_btn = '<button id="close_feedback" class="close_feedback" onclick="navigation.hide_feedback(this);">Close</button><p>';
// echo out positive messages
if (isset($feedback_positive)) {
    foreach ($feedback_positive as $feedback) {
        echo $feedback_template_pre.$feedback_template_close_btn.$feedback.$feedback_template_post;
    }
}

// echo out negative messages
if (isset($feedback_negative)) {
    foreach ($feedback_negative as $feedback) {
        echo $feedback_template_pre.$feedback_template_close_btn.$feedback.$feedback_template_post;
    }
}