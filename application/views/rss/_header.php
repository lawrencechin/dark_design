<?php ?>
<nav class="secondary_header">
	<ul>
		<li class="no_padding"><button disabled title="Reveal Sidebar" class="sidebarReveal" id="sidebarReveal" onclick="utils.sidebarRevealMobile(this);" onclick="utils.sidebarRevealMobile();"></button></li><?php
		?><li <?php if($this->checkForActiveControllerAndAction($filename, 'rss/index')){echo 'class="active"';}?>
		><span><a href="<?=URL.'rss/index'?>">View Feeds</a></span></li><?php
		?><li><span <?php if($this->checkForActiveControllerAndAction($filename, 'rss/editFeeds')){echo 'class="active"';}?>
		><a href="<?=URL.'rss/editFeeds'?>">Edit Feeds</a></span></li><?php
		?><li class="no_padding"><button title="Add Feed" onclick="form_actions.revealForm(this);" class="add_feed"></button></li><?php
		?><li class="no_padding"><button title="Add Bookmark" class="add_bookmark" onclick="form_actions.revealForm(this)"></button></li><?php
		?><li class="no_padding"><button title="Refresh Feeds" disabled="disabled" onclick="rss.refresh_feeds();" id="refresh_feeds" class="refresh_feeds"></button></li>

		<form onsubmit="form_actions.submitForm(this, event);" method="post" action="<?=URL.'rss/add'?>" class="add_feed" name="add_feed">
			<h3>Add Feed</h3>
			<label>
				<span>Feed URL</span>
				<input name="feed_url" type="url" placeholder="url…" autocomplete="off" required>
			</label>

			<label>
				<span>Tags</span>
				<ul class="tag_entry">
					<li class="add_new_tag"><button type="button" title="Add New Tag" onclick="form_actions.addTag(this, '');">+</button></li>
				</ul>
			</label>

			<button title="Submit" class="submit" type="submit"></button><?php
			?><button title="Cancel" onclick="form_actions.cancelSubmission(this); return false;" class="cancel"></button>
		</form>
	</ul>
</nav>