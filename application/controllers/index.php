<?php

/**
 * Index Controller
 * 
 **/

class Index extends Controller{
	function __construct(){
		parent::__construct();
	}

	//homepage
	function index($tag = NULL){
		Auth::handleLogin();
		$tagArr = $tag !== NULL && preg_match('~^[0-9]+(,[0-9]+)*$~i', $tag)? explode(',', $tag) : $tag;
		//grab all data related to search and display on this page
		//or grab the last 10 entries in each segment if no search supplied

		//only need one model as the functions are universal
		$book_model = $this->loadModel('Bookmark');

		if(!is_dir(NON_HTTP_PATH.CONTENT_DIR)){
			$book_model->contentOffline();
		}
		//get all bookmarks, images, stars & videos for a specific tag (all user content - not external searches)
		$this->view->book = $book_model->getMedia('b', 1, 10, $tagArr);
		$this->view->images = $book_model->getMedia('i', 1, 10, $tagArr);
		$this->view->feeds = $book_model->getMedia('f', 1, 10, $tagArr);
		
		if($tagArr !== NULL && $tagArr !== 'tl' ){
			$this->view->search = str_replace('_', ' ', $tagArr);
			$this->view->tags = is_array($tagArr) ? $book_model->getTags(NULL, $tagArr) : $this->view->tags = $book_model->getTags(NULL);
		}

		$this->view->render('index/index');
	}
}