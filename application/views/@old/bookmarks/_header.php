<nav class="secondary_header">
	<ul>
		<li class="no_padding left_border"><button title="Add Bookmark" class="add_bookmark" onclick="form_actions.revealForm(this)"></button></li>

		<li class="no_padding"><button title="Edit Multiple Tags" disabled="disabled" onclick="form_actions.revealForm(this);" class="edit_multi"></button></li>
		<li class="no_padding"><button title="Select All/Deselect Selected" disabled="disabled" onclick="form_actions.checkAll(this, '#bookmarks_list');" id="select_all" class="check_image no_margin"></button></li>
	</ul>
	
	<form onsubmit="form_actions.submitForm(this, event);" method="post" action="<?=URL.'bookmarks/create'?>" class="add_bookmark" name="add_bookmark">

		<label>
			<span>Bookmark Title</span>
			<input name="book_title" type="text" placeholder="title…" autocomplete="off">
		</label>

		<label>
			<span>Bookmark URL</span>
			<input name="book_url" type="url" placeholder="url…" autocomplete="off" required>
		</label>

		<label>
			<span>Tags</span>
			<ul class="tag_entry">
				<li class="add_new_tag"><button type="button" title="Add New Tag" onclick="form_actions.addTag(this, '');">+</button></li>
			</ul>
		</label>

		<button title="Submit" class="submit" type="submit"></button>
		<button title="Cancel" onclick="form_actions.cancelSubmission(this); return false;" class="cancel"></button>
	</form>

	<form onsubmit="form_actions.submitForm(this, event);" method="post" action="<?=URL.'bookmarks/editMulti'?>" class="edit_multi" name="edit_multi" id="edit_multi">
		<h3>Edit Multiple Tags <sub>(for selected images)</sub></h3>
		<label>
			<span>Remove current tags? <sub>Current tags will be removed and new tags added.</sub></span>
			<input type="checkbox" name="remove_tags" class="remove_tags">
		</label>

		<label>
			<span>Tags</span>
			<ul class="tag_entry">
				<li class="add_new_tag"><button type="button" title="Add New Tag" onclick="form_actions.addTag(this, '');">+</button></li>
			</ul>
		</label>

		<button title="Submit" class="submit" type="submit"></button>
		<button type="button" title="Cancel" onclick="form_actions.cancelSubmission(this); return false;" class="cancel"></button>
	</form>
</nav>