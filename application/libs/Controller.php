<?php
/**
* Base level controller. All other controllers extend from this 
*
**/	

class Controller{
	function __construct(){
		//start session
		Session::init();

		//user has 'remember me cookie' then login with cookie
		if(!isset($_SESSION['user_logged_in']) && isset($_COOKIE['rememberme'])){
			Application::app_new_location('login/loginWithCookie');
		}

		//create database connection
		try{
			$this->db = new Database();
		}catch(PDOException $e){
			die('Database connection could not be established');
		}

		//create a view object (that does nothing, but provides the view render() method)
		$this->view = new View();
		$this->edit_able = (int)Session::get('user_edit') === 1? true : false;
	}

	//loads model with specified name
	public function loadModel($name){
		$path = MODELS_PATH . strtolower($name) . '_model.php';
		if(file_exists($path)){
			require MODELS_PATH . strtolower($name) . '_model.php';
			$modelName = $name . 'Model';
			//return model object with database connection
			return new $modelName($this->db);
		}
	}

	public function addTag(){
		if($this->edit_able){
			if(isset($_POST['tag_title']) && !empty($_POST['tag_title']) && isset($_POST['media_id']) && !empty($_POST['media_id'])){
				$tagTitle = array($_POST['tag_title']);
				$media_id = $_POST['media_id'];
				$media_type = isset($_POST['media_type']) && !empty($_POST['media_type']) ? $_POST['media_type'] : $this->setMediaType();
				$book_model = $this->loadModel('Bookmark');
				$result = $book_model->tags($tagTitle, $media_type, $media_id);
				
				//set responses based on media_type 
				switch ($media_type){
					case 'b' : 
					$url_1 = 'bookmarks/index/';
					$url_2 = 'bookmarks/removeTag/';
					$media_type = 'b';
					break;

					case 'f' :
					$url_1 = 'rss/index/';
					$url_2 = 'rss/removeTag/';
					$media_type = 'f';
					break;

					case 'n' :
					$url_1 = 'images/getImages/1/';
					$url_2 = 'images/removeTag/';
					$media_type = 'n';
					break;

					case 'i' : 
					$url_1 = 'images/getImages/1/';
					$url_2 = 'images/removeTag/';
					$media_type = 'i';
					break;
				}

				if($result[0][1] > 0){
					echo json_encode(array('msg' => 'Tag_Added', 'error' => false, 'tag_id' => $result[0][0]->tag_id, 'tag_title' => $result[0][0]->tag_title, 'url_1' => $url_1, 'url_2' => $url_2, 'media_type' => $media_type));
				}else{
					echo json_encode(array('msg' => 'Tag_Not_Added', 'error' => true));
				}
			}else{
				echo json_encode(array('msg' => 'Tag_Not_Added', 'error' => true));
			}
		}else{
			echo json_encode(array('msg' => 'Disabled in demo', 'error' => true));
		}
    }

    public function removeTag($tag_id = NULL, $media_id = NULL, $media_type = NULL){
    	if($this->edit_able){
    		if(isset($tag_id) && $tag_id !== NULL && isset($media_id) && $media_id !== NULL){
				$tag_id = (int)$tag_id;
				$media_id = (int)$media_id;
				$media_type = $media_type === NULL ? $this->setMediaType() : $media_type;
				$book_model = $this->loadModel('Bookmark');
				$result = $book_model->removeTag($tag_id, $media_id, $media_type);

				if($result > 0){
					echo json_encode(array('error' => false));
				}else{
					echo json_encode(array('error' => true));
				}
			}else{
				echo json_encode(array('error' => true));
			}
    	}else{
    		echo json_encode(array('error' => true));
    	}
	}

	public function editMulti(){
		if($this->edit_able){
			//tags can be empty, ID's must not
			if(isset($_POST['tags']) && isset($_POST['ID']) && !empty($_POST['ID'])){
				$book_model = $this->loadModel('Bookmark');
				$tags = !empty($_POST['tags']) ? explode(',', $_POST['tags']) : NULL;
				$id = explode(',', $_POST['ID']);
				//check for chekbox and then for valid tag list to remove
				$remove = isset($_POST['remove_tags']) && !empty($_POST['remove_tags']) ? isset($_POST['tag_removal']) && !empty($_POST['tag_removal']) ? explode(',', $_POST['tag_removal']) : false : false;
				$result = $book_model->groupEditTags($tags, $id, $this->setMediaType(), $remove);

				if($result){
					echo json_encode(array('error' => false, 'msg' => 'Tags_Updated'));
				}else{
					echo json_encode(array('error' => true, 'msg' => 'Tags_Not_Updated', 'other' => 'bad result'));
				}
			}else{
				echo json_encode(array('error' => true, 'msg' => 'Tags_Not_Updated', 'other' => 'didnt recieve correct data'));
			}
		}else{
			echo json_encode(array('error' => true, 'msg' => 'Disabled in demo'));
		}
	}
}