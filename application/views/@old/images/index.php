<section class="images img_main">
<?php 
$this->sidebar('i');
$this->sidebarRevealBtn();
$this->search(); 
$Parsedown = !empty($this->notes) ? new Parsedown() : NULL;
?>

<ul id="image_content" class="red_content image_content">
<?php 
	//notes
	if($this->notes && count($this->notes) > 1){
		foreach($this->notes as $notes){ 
			if(isset($notes->count)){/**ignore*/}else{?>
			<li class="notes_md">
				<img onerror="imgError(this)" width="200px" height="250px" src="<?=URL.PUBLIC_IMAGES.'background.gif'?>">
				<div class="note_parsed_content">
				<?=$Parsedown->text($notes->content)?></div>
				<textarea class="edit_note_ta" name="edit_note_ta" data-mediatype="n" required></textarea>
				<ul class="tag_entry">
					<sub>tags: </sub>
					<?php 
					$count = 0;
					if(!empty($notes->tag_id)){
						$tags = explode(',', $notes->tag_id);
						$tag_titles = explode(',', $notes->tag_title);
						foreach($tags as $tags){
							echo '<li class="tag_list_item"><a href="'.URL.'images/getImages/1/'.$tags.'">'.$tag_titles[$count].'</li></a><a href="'.URL.'images/removeTag/'.$tags.'/'.$notes->ID.'/n" class="tag_delete"><span class="removeFav"></span></a>';
							$count++;
						}
					}?>
					<li class="add_new_tag"><button title="Add New Tag" data-id="<?=$notes->ID?>" data-mediatype="n" onclick="form_actions.addTag(this, '<?=URL."images/addTag"?>');">+</button></li>
				</ul>	

				<div class="button_container">
					<button title="Edit Note" data-id="<?=$notes->ID?>" data-posturl="<?=URL.'images/editNote/'?>" onclick="form_actions.revealFormInline(this); notes.noteEdit(this);" class="edit_note"></button>
					<button title="Delete Note" onclick="form_actions.deleteFormInline(this);" class="delete_note"></button>
				</div>

				<form onsubmit="form_actions.submitForm(this, event);" method="post" action="<?=URL.'images/deleteNote'?>" class="delete_note" name="delete_note">
					<input name="note_id" type="hidden" value="<?=$notes->ID?>">
					<button title="Delete Note" type="submit">delete</button>
				</form>
			</li>
		<?php } }
	}

	if($this->images && count($this->images) > 1){
		foreach($this->images as $images){
			if(isset($images->count)){/**ignore*/}else{
				$thumbName = preg_replace('/\\.[^.\\s]{2,5}$/', '', $images->IMGname);;
				$thumbPath = URL.CONTENT_DIR.$images->IMGpath.'thumb/'.$thumbName.'.jpg';
		?>
		<li>
			<a href="<?=URL.CONTENT_DIR.$images->IMGpath.$images->IMGname?>"><img onerror="imgError(this)" width="200px" height="250px" src="<?=$thumbPath?>"></a>

			<ul class="tag_entry">
				<sub>tags: </sub>
				<?php 
				$count = 0;
				if(!empty($images->tag_id)){
					$tags = explode(',', $images->tag_id);
					$tag_titles = explode(',', $images->tag_title);
					foreach($tags as $tags){
						echo '<li class="tag_list_item"><a href="'.URL.'images/getImages/1/'.$tags.'">'.$tag_titles[$count].'</li></a><a href="'.URL.'images/removeTag/'.$tags.'/'.$images->ID.'" class="tag_delete"><span class="removeFav"></span></a>';
						$count++;
					}
				}?>
				<li class="add_new_tag"><button title="Add New Tag" data-id="<?=$images->ID?>" onclick="form_actions.addTag(this, '<?=URL."images/addTag"?>');">+</button></li>
			</ul>			

			<div class="button_container">
				<button title="Select Image" onclick="form_actions.select_image(this, '.img_main');" class="check_image" data-id="<?=$images->ID?>"></button>
				<button title="Edit Image" onclick="form_actions.revealFormInline(this);" class="edit_image"></button>
				<button title="Delete Image" onclick="form_actions.deleteFormInline(this);" class="delete_image"></button>
			</div>

			<form onsubmit="form_actions.submitForm(this, event);" method="post" action="<?=URL.'images/deleteImages'?>" class="delete_image" name="delete_image">
				<input name="ID" type="hidden" value="<?=$images->ID?>">
				<button title="Submit" type="submit">delete</button>
			</form>
		</li>
<?php } }?>
</ul>
<?php 
$this->pagination($this->images['count']->count);
}
if((!$this->notes || count($this->notes) == 1) && (!$this->images || count($this->images) == 1)){
	$this->no_content('Somthing is amiss…', 'No images, add some!');
}
?>
</section>
