<?php

class Bookmarks extends Controller{

	public function __construct(){
		parent::__construct();
		Auth::handleLogin();
	}

	public function index($tag = NULL){
		//check tags are set and check if the values contain only numbers, commas with no trailing comma - convert to array
		$tagArr = $tag !== NULL && preg_match('~^[0-9]+(,[0-9]+)*$~i', $tag)? explode(',', $tag) : $tag;
		$book_model = $this->loadModel('Bookmark');
		$this->view->book = $book_model->getMedia('b', NULL, 0, $tagArr);
		$this->view->tags = is_array($tagArr) ? $book_model->getTags('b', $tagArr) : $this->view->tags = $book_model->getTags('b');
		if($tagArr !== NULL && $tagArr !== 'tl' ){
			$this->view->search = str_replace('_', ' ', $tagArr);
		}
		
		$this->view->render('bookmarks/index');
	}


	public function create(){
		if($this->edit_able){
			//check POST vars
			if(isset($_POST['book_title']) 
			&& !empty($_POST['book_title']) 
			&& isset($_POST['book_url']) 
			&& !empty($_POST['book_url'])
			&& isset($_POST['tags'])
			){
				//convert tags into an array
				$tags = !empty($_POST['tags']) ? explode(',', $_POST['tags']) : NULL;
				$book_model = $this->loadModel('Bookmark');
				$result = $book_model->create($_POST['book_title'], $_POST['book_url'], $tags);	

				if($result){
					if(isset($_POST['book_rss'])){
						echo json_encode(array('msg'=>'RSS_Bookmark_Added', 'error'=>false));
					}else{
						echo json_encode(array('msg'=>'Bookmark_Added', 'error'=>false));
					}
				}
			}
		}else{
			echo json_encode(array('msg'=>'Disabled in demo', 'error'=>true));
		}
	}

	public function edit(){
		if($this->edit_able){
			//check POST vars
			if(isset($_POST['book_name']) && !empty($_POST['book_name']) && isset($_POST['book_id']) && !empty($_POST['book_id'])){
				$book_model = $this->loadModel('Bookmark');
				$result = $book_model->edit($_POST['book_id'], $_POST['book_name']);
				if($result > 0){
					echo json_encode(array('error' => false, 'msg' => 'Title_Updated', 'title' => $_POST['book_name']));
				}else{
					echo json_encode(array('error' => true, 'msg' => 'Title_NOT_Updated'));
				}
			}else if(isset($_POST['book_url']) && !empty($_POST['book_url']) && isset($_POST['book_id']) && !empty($_POST['book_id'])){
				$book_model = $this->loadModel('Bookmark');
				$result = $book_model->edit($_POST['book_id'], NULL, $_POST['book_url']);
				if($result > 0){
					echo json_encode(array('error' => false, 'msg' => 'URL_Updated', 'url' => $_POST['book_url']));
				}else{
					echo json_encode(array('error' => true, 'msg' => 'URL_NOT_Updated'));
				}
			}else{
				echo json_encode(array('error' => true, 'msg' => 'URL_NOT_Updated'));
			}
		}else{
			echo json_encode(array('error' => true, 'msg' => 'Disabled in demo'));
		}
	}

	public function delete(){
		if($this->edit_able){
			if(isset($_POST['book_id']) && !empty($_POST['book_id']) || isset($_POST['book_ids']) && !empty($_POST['book_ids'])){

				if(isset($_POST['book_ids']) && !empty($_POST['book_ids'])){
					$data = $_POST['book_ids'];
				}else{
					$data = $_POST['book_id'];
				}
				$book_model = $this->loadModel('Bookmark');
				$test = $book_model->delete($data);
				if($test[0] && $test[1]){
					echo json_encode(array('msg'=>'Bookmark_Deleted_Multi', 'error'=>false));
				}else if($test[0] && !$test[1]){
					echo json_encode(array('msg'=>'Bookmark_Deleted', 'error'=>false));
				}else{
					echo json_encode(array('msg'=>'Bookmark_Not_Deleted', 'error'=>true));
				}
			}else{
				echo json_encode(array('msg'=>'Bookmark_Not_Deleted', 'error'=>true));
			}
		}else{
			echo json_encode(array('msg'=>'Bookmark_Deleted', 'error'=>false));
		}
	}

	public function setMediaType(){
		return 'b';
	}	
}