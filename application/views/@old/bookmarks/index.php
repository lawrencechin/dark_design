<section class="bookmarks">
<?php 
$this->sidebar('b');	
$this->sidebarRevealBtn();
$this->search();

if($this->book && count($this->book) > 1){ ?>
<ul id="bookmarks_list" class="vertical_list book">

<?php foreach($this->book as $bookmark){ 
	if(isset($bookmark->count)){/**ignore*/}else{?>
	<li class="filter_book">
		<sub>title: </sub><h2 data-info="<?=$bookmark->ID?>" data-postURL="<?=URL.'bookmarks/edit/'?>" class="light_heading editable"><a target="_blank" href="<?=$bookmark->URL?>"><?=$bookmark->Title?></a></h2>

		<input autocomplete="off" class='edit_name' name='name' type='text' value="<?=$bookmark->Title?>" placeholder='<?=$bookmark->Title?>' data-id='<?=$bookmark->ID?>' data-postURL='<?=URL.'bookmarks/edit/'?>' onblur='form_actions.submitEdit(this)'>
		<sub class="hidden_label">url: </sub><input autocomplete="off" class='edit_name' name='url' type='url' value="<?=$bookmark->URL?>" placeholder='<?=$bookmark->URL?>' data-id='<?=$bookmark->ID?>' data-postURL='<?=URL.'bookmarks/edit/'?>' onblur='form_actions.submitEdit(this)' required>

		<ul class="tag_entry">
			<sub>tags: </sub>
			<?php 
			$count = 0;
			if(!empty($bookmark->tag_id)){
				$tags = explode(',', $bookmark->tag_id);
				$tag_titles = explode(',', $bookmark->tag_title);
				foreach($tags as $tags){
					echo '<li class="tag_list_item"><a href="'.URL.'bookmarks/index/'.$tags.'">'.$tag_titles[$count].'</li></a><a href="'.URL.'bookmarks/removeTag/'.$tags.'/'.$bookmark->ID.'" class="tag_delete"><span class="removeFav"></span></a>';
					$count++;
				}
			}?>
			<li class="add_new_tag"><button title="Add New Tag" data-id="<?=$bookmark->ID?>" onclick="form_actions.addTag(this, '<?=URL."bookmarks/addTag"?>');">+</button></li>
		</ul>		
		
		<div class="button_container">
			<button title="Select Bookmark" onclick="form_actions.select_image(this,'.bookmarks');" class="check_image" data-id="<?=$bookmark->ID?>"></button>
			<button title="Edit Bookmark" onclick="form_actions.revealFormInline(this);" class="edit_bookmark"></button>
			<button title="Delete Bookmark" onclick="form_actions.deleteFormInline(this);" class="delete_bookmark"></button>
		</div>
		
		<form onsubmit="form_actions.submitForm(this, event);" method="post" action="<?=URL.'bookmarks/delete'?>" class="delete_bookmark" name="delete_bookmark">
			<input name="book_id" type="hidden" value="<?=$bookmark->ID?>">
			<button type="submit">delete</button>
		</form>
	</li>
<?php }
}
echo '</ul>';
}else{
	$this->no_content('No Bookmarks', 'Add some bookmarks via <i><b>+ button</b></i> in the menu above');
} ?>
</section>
