<section class="images">
<?php 
if(!empty($this->tags)){ ?>
	<div class="searchAndSort">
		<button class="sortOrder" onclick="search.sortList(this, 'image_tag_list', 'nameASC');" title="Sort Name Ascending">abc</button>
		<button class="sortOrder" onclick="search.sortList(this, 'image_tag_list', 'nameDSC');" title="Sort Name Descending">zyx</button>
		<button class="sortOrder" onclick="search.sortList(this, 'image_tag_list', 'countASC');" title="Sort Count Ascending">123</button>
		<button class="sortOrder active" onclick="search.sortList(this, 'image_tag_list', 'countDSC');" title="Sort Count Descending">321</button>
		<button class="sortOrder" onclick="search.sortList(this, 'image_tag_list', 'rand');" title="Sort Randomly">Random</button>
		<input id="search_tags" class="search_tags" placeholder="search tags…" type="text">
	</div>
	<ul class="image_tag_list tag_entry">

	<li class="tag_list_item">
		<span class="tag_count"><button data-id="tl" onclick="navigation.preview(this);" class="preview_btn" title="Preview"></button></span>
		<a href="<?=URL.'images/getImages/1/tl'?>">
		<h3>Images without Tags</h3></a>
	</li>

	<li class="tag_list_item">
		<span class="tag_count">All<button data-id="" onclick="navigation.preview(this);" class="preview_btn" title="Preview"></button></span>
		<a href="<?=URL.'images/getImages/1/'?>">
		<h3>Latest Images</h3></a>
	</li>

	<li class="tag_list_item">
		<span class="tag_count">Notes</span>
		<a href="<?=URL.'images/getNotes/1/'?>">
		<h3>Notes</h3></a>
	</li>

	<?php foreach($this->tags as $key=>$tags){ ?>
		<li class="tag_list_item">
			<span class="tag_count"><?=$tags['count']?><button title="Preview" data-id="<?=$key?>" onclick="navigation.preview(this);" class="preview_btn" title="Preview"></button></span>
			<a href="<?=URL.'images/getImages/1/'.$key?>">
			<h3><?=$tags['title']?></h3></a>
		</li>
	<?php }
	echo '</ul>';
}else{
	$this->no_content('No images', 'Add some files via the <b><i>add files</i><b> menu');
}?>
</section>
