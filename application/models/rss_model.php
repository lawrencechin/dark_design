<?php

class RssModel extends MasterModel{

	public function __construct(Database $db){
		parent::__construct($db);
	}

	public function add($url, $tags){
		$sql = "INSERT INTO Feed_URLS(URL, user_id) VALUES (:URL, :user_id)";

		$dataArr = array(
			'URL' => $url,
			'user_id' => $_SESSION['user_id']
			);

		$result = $this->commitDb($sql, $dataArr, false, true);

		if($result){
			if($tags !== NULL){
				$this->tags($tags, 'f', $result);
				if($result){
					return true;
				}
				return false;
			}else{
				//done - no tagging
				return true;
			}
		}

		return false;
	}

	public function edit($url, $id){
		$sql = "UPDATE Feed_URLS SET URL = :URL WHERE ID = :ID AND user_id = :user_id";
		$dataArr = array(
			'URL' => $url,
			'ID' => $id,
			'user_id' => $_SESSION['user_id']
			);

		return $this->commitDb($sql, $dataArr, false);
	}

	public function delete($id){
		$sql = "DELETE FROM Feed_URLS WHERE ID = :ID AND user_id = :user_id";
		$dataArr = array('ID' => $id, 'user_id' => Session::get('user_id'));

		if($this->commitDb($sql, $dataArr, false) > 0){
			//now we will delete all rows from rss table belonging to feed url and current user
			$sql = "DELETE FROM Feed_List WHERE URLID = :URLID AND user_id = :user_id";
			$dataArr = array('URLID' => $id, 'user_id' => Session::get('user_id'));
			$this->commitDb($sql, $dataArr, false);
			//feeds deleted, now let's remove tag associations
			$this->removeTag(NULL, $id, 'f');
			return true;
		}
	}

	public function getContent($ID){
		$sql = "SELECT Title, PostDate, Content, Perma FROM Feed_List WHERE ID = :ID AND user_id = :user_id";
		$dataArr = array('ID' => $ID, 'user_id' => $_SESSION['user_id']);
		return $this->commitDb($sql, $dataArr, true);
	}

	public function markSingleRead($id){
		$sql = "UPDATE Feed_List SET feed_read = 1 WHERE ID = :ID AND user_id = :user_id";
		$dataArr = array('ID' => $id, 'user_id' => $_SESSION['user_id']);

		return $this->commitDb($sql, $dataArr, false);
	}

	public function markDayRead(){
		$user_id = $_SESSION['user_id'];
		$yesterday  = date("Y-m-d H:i", strtotime("-1 day"));
		$sql = "UPDATE Feed_List SET feed_read = 1 WHERE sortDate < '$yesterday' AND user_id = $user_id";

		try{
			$this->db->exec($sql);
			return true;
		}catch(Exception $e){
			return false;
		}
	}

	public function markWeekRead(){
		$user_id = $_SESSION['user_id'];
		$lastWeek  = date("Y-m-d H:i", strtotime("-1 week"));
		$sql = "UPDATE Feed_List SET feed_read = 1 WHERE sortDate < '$lastWeek' AND user_id = $user_id";

		try{
			$this->db->exec($sql);
			return true;
		}catch(Exception $e){
			return false;
		}
	}

	public function markMonthRead(){
		$user_id = $_SESSION['user_id'];
		$lastMonth  = date("Y-m-d H:i", strtotime("-1 month"));
		$sql = "UPDATE Feed_List SET feed_read = 1 WHERE sortDate < '$lastMonth' AND user_id = $user_id";

		try{
			$this->db->exec($sql);
			return true;
		}catch(Exception $e){
			return false;
		}
	}

	public function markAllRead(){
		$user_id = $_SESSION['user_id'];
		$sql = "UPDATE Feed_List SET feed_read = 1 WHERE user_id = $user_id";

		try{
			$this->db->exec($sql);
			return true;
		}catch(Exception $e){
			return false;
		}
	}

	private function truncateDescription($input, $numwords, $padding = ""){
		$output = strtok($input, " ");
	    while(--$numwords > 0) $output .= " " . strtok(" ");
	    if($output != $input) $output .= $padding;
	    return $output;
	}

	public function getURLS(){
		$sql = "SELECT ID, URL FROM Feed_URLS WHERE user_id = :user_id";
		$dataArr = array('user_id' => $_SESSION['user_id']);
		$result = $this->commitDb($sql, $dataArr, true);

		return $result;
	}

	public function refreshFeeds(){
		$urlArr = array();
		$feedArr = array();

		foreach($this->getURLS() as $URLS){
			$urlArr[] = $URLS->URL;
			$feedArr[] = $URLS;
		}

		$feed = new SimplePie();
		$feed->set_feed_url($urlArr);
		$feed->set_cache_location(NON_HTTP_PATH . '/application/cache');
		$success = $feed->init();
		$feed->handle_content_type();

		if($success){

			foreach($feed->get_items() as $item){
				$title = $item->get_title();
				$id = $item->get_id(true);
				$date = $item->get_date();
				$sqlDate = empty($item->get_date("Y-m-d H:i"))? date("Y-m-d H:i") : $item->get_date("Y-m-d H:i");
				$description = $this->truncateDescription(strip_tags($item->get_description(true)), 30, "...");
				$content = $item->get_content();
				$perma = $item->get_permalink();
				//Get feed url to match against feed array
				$sourceURL = $item->get_feed()->subscribe_url();

				//Loop through feed array and match feed ID to feed item
				foreach($feedArr as $feedInfo){
					if(strcmp($sourceURL, $feedInfo->URL) === 0){
						$URLID = $feedInfo->ID;
					}
				}
				$user_id = $_SESSION['user_id'];

				$dataArr = array("ID" => $id, "PostDate" => $date, "Title" => $title, "Description" => $description , "Content" => $content, 'feed_read' => 0, 'Perma' => $perma, 'URLID' => $URLID, 'sortDate' => $sqlDate, 'user_id' => $user_id);

				$sql = "INSERT INTO Feed_List(ID, PostDate, Title, Description, Content, feed_read, Perma, URLID, sortDate, user_id) VALUES(:ID, :PostDate, :Title, :Description, :Content, :feed_read, :Perma, :URLID, :sortDate, :user_id) ON DUPLICATE KEY UPDATE ID = ID";

				$this->commitDb($sql, $dataArr, false);
			}

		    /**$lastWeek = date("Y-m-d H:i", strtotime("-1 week"));
			$clearDb = "UPDATE Feed_List SET PostDate = '', Title = '', Description = '', Content = '', Perma = '', URLID = 0, sortDate = '' WHERE sortDate < '$lastWeek' AND feed_read = 1";*/

			$clearDb = "DELETE FROM Feed_List WHERE feed_read = 1"; 

			$this->db->exec($clearDb);
			return true;
		}else{
			return false;
		}
	}

	public function exportFeeds($feeds, $dir){
		if(isset($feeds) && count($feeds) > 1){
			$date = date("r");
			$name = Session::get('user_name');
			$email = Session::get('user_email');
			$filename = 'feeds_export.opml';
			
			$xml = new XMLWriter();
			$xml->openMemory();
			$xml->setIndent(true);
			$xml->startDocument('1.0','UTF-8');
			//start OPML
			$xml->startElement("opml");
		    $xml->writeAttribute("version", "1.0");
		    //add head attributes
			$xml->startElement("head");
			//title
			$xml->startElement('title');
			$xml->text('Dark Design Feed Export');
			$xml->endElement();
			//date created
			$xml->startElement('dateCreated');
			$xml->text($date);
			$xml->endElement();
			//owner name
			$xml->startElement('ownerName');
			$xml->text($name);
			$xml->endElement();
			//owner email
			$xml->startElement('ownerEmail');
			$xml->text($email);
			$xml->endElement();
			//end head
			$xml->endElement();
			//start body
			$xml->startElement('body');
			//loop through and add the outlines
			foreach($feeds as $fu){
				if(!isset($fu->count)){
					$data = $this->getFeedData($fu->URL);
					if(!empty($data)){
						$xml->startElement('outline');
						$xml->writeAttribute('htmlUrl', $data['link']);
						$xml->writeAttribute('xmlUrl', $data['url']);
						$xml->writeAttribute('title', $data['title']);
						$xml->writeAttribute('text', $data['title']);
						$xml->writeAttribute('type', 'rss');
						$xml->endElement();
					}
				}
			}
			//end body
			$xml->endElement();
			//end OPML
			$xml->endElement();

            $handle = fopen($dir . $filename, 'w');
            fwrite($handle, $xml->outputMemory(true));
            fclose($handle);
			return array('success' => true, 'path' => $dir, 'filename' => $filename);
		}else{
			return array('success' => false);
		}
	}

	private function getFeedData($url){
		$feed = new SimplePie();
		$feed->set_feed_url($url);
		$feed->set_cache_location(NON_HTTP_PATH . '/application/cache');
		$success = $feed->init();
		$feed->handle_content_type();
		$title = $feed->get_title();
		$link = $feed->get_link();
		return array('title' => $title, 'link' => $link, 'url' => $url);
	}

	public function properTagCounts(){
		$sql = "SELECT tlt.tag_id, Count(tlt.tag_id) as count FROM Feed_List f LEFT JOIN Feed_URLS fu on f.URLID = fu.ID LEFT JOIN tag_link_table tlt ON fu.ID = tlt.media_id AND tlt.media_type = 'f' AND tlt.user_id = :user_id  WHERE f.user_id = :user_id AND f.feed_read = 0 Group By tlt.tag_id";
		$dataArr = ['user_id' => $_SESSION['user_id']];
		$tempArr =  $this->commitDb($sql, $dataArr, true);
		$finalArr = [];
		foreach($tempArr as $tags){
			$finalArr[$tags->tag_id] = $tags->count;
		}

		return $finalArr;
	}
}

?>

