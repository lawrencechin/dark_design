<?php include_once '_head.php' ?>
<body>
    <header class="header" id="header">
        <?php if(Session::get('user_logged_in')){?>
        <nav class="main_nav">
            <ul class="clearfix">
                <li class="left <?php if($this->checkForActiveController($filename, 'login')){echo 'active';}?>">
                <span class="header_user_name"><a href="<?=URL?>index"><?=Session::get('user_name') ?></a></span></li>
                <li class="right"><span class="logout"><a href="<?=URL?>login/logout">Logout</a></span></li>
                 
                <li <?php if($this->checkForActiveController($filename, 'bookmarks')){echo 'class="active"';}?>
                ><a href="<?=URL?>bookmarks/">Bookmarks</a></li>
                <li <?php if($this->checkForActiveController($filename, 'images')){echo 'class="active"';}?>
                ><a href="<?=URL?>images/">Images</a></li>
                <li <?php if($this->checkForActiveController($filename, 'rss')){echo 'class="active"';}?>
                ><a href="<?=URL?>rss/">Rss</a></li>

                <li><input id="search" class="search mousetrap" placeholder="search all by tag…">
                <button id="submit_search" onclick="search.submit_search(this);" class="submit_search"></button>
                </li>

            </ul>  
        </nav>

        <?php
            if($this->checkForActiveController($filename, 'bookmarks')){
                include_once NON_HTTP_PATH.VIEWS_PATH.'bookmarks/_header.php';
            }else if($this->checkForActiveController($filename, 'images')){
                include_once NON_HTTP_PATH.VIEWS_PATH.'images/_header.php';
            }else if($this->checkForActiveController($filename, 'rss')){
                include_once NON_HTTP_PATH.VIEWS_PATH.'rss/_header.php';
            }
        }?>
    </header>