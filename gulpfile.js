//Dependencies
var gulp = require('gulp');
var cssnano = require('gulp-cssnano');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var autoprefixer = require('gulp-autoprefixer');
var sass = require('gulp-sass');

//Options
opts = {
	autoprefixer : {
		browsers : 'last 2 versions',
		cascade : false
	},
	minRename : {
		suffix : '.min'
	},
	sassPath : './public/css/sass/*.scss',
	cssDest : './public/css/min/',
	jsPath : './public/js/fragments/*.js',
	jsDest : './public/js/min/'
};

//uglify function
function jsUg(file){
	gulp.src([file])
		.pipe(uglify())
		.pipe(rename(opts.minRename))
		.pipe(gulp.dest(opts.jsDest));
}
//sass tasks
gulp.task('sassBuild', function(){
	gulp.src(opts.sassPath)
		.pipe(sass().on('error', sass.logError))
		.pipe(autoprefixer(opts.autoprefixer))
		.pipe(cssnano())
		.pipe(rename(opts.minRename))
		.pipe(gulp.dest(opts.cssDest));
});
//default / watch function
gulp.task('default', function(){
	gulp.watch(opts.jsPath).on('change', function(event){
		jsUg(event.path);
	});

	gulp.watch(opts.sassPath, ['sassBuild']);
});