<?php ?>
<nav class="secondary_header">
	<ul>
		<li <?php if($this->checkForActiveControllerAndAction($filename, 'images/temp')){echo 'class="active"';}?>
		><span><a href="<?=URL.'images/temp/'?>">Temp Files</a></span></li>
		
		<li class="no_padding"><button title="Add Images" onclick="form_actions.revealForm(this); notes.clearRestoreForm('#add_note', true);" class="add_files"></button></li>

		<li class="no_padding"><button title="Move Temp Images" onclick="form_actions.revealForm(this); temp.moveFiles(this);" disabled="disabled" class="move_temp_files"></button></li>

		<li class="no_padding"><button title="Delete Selected Images" disabled="disabled" onclick="temp.moveFiles(this);" class="delete_temp_files"></button></li>

		<li class="no_padding"><button title="Edit Image Tags" disabled="disabled" onclick="form_actions.revealForm(this);" class="edit_multi"></button></li>

		<li class="no_padding"><button title="Select All/Deselect Selected" disabled="disabled" onclick="form_actions.checkAll(this, '#image_content');" id="select_all" class="check_image no_margin"></button></li>
	</ul>

	<?php //Add File form ?>

	<form onsubmit="form_actions.submitForm(this, event);" method="post" id="add_files" action="<?=URL.'images/addImages'?>" class="add_files" name="add_files" enctype="multipart/form-data">
		<input type="hidden" name="MAX_FILE_SIZE" value="5242880000" />
		<h3>Add Files / <a href='#note' class='add_note_toggle'>Add a Note</a></h3>
		<label>
			<span><sub>Files: </sub>Select Files to Upload</span>
			<input name="img_files[]" type="file" multiple="true" value="" accept="image/jpeg, image/jpg, image/gif, image/png, image/tiff" required>
		</label>
		<label>
			<span>Upload to the <b>Temp</b> folder and sort later? <sub>Tags have no effect if selected.</sub></span>
			<input type="checkbox" name="upload_to_temp" class="upload_to_temp">
		</label>
		<label>
			<span>Tags</span>
			<ul class="tag_entry">
				<li class="add_new_tag"><button type="button" title="Add New Tag" onclick="form_actions.addTag(this, '');">+</button></li>
			</ul>
		</label>

		<button title="Submit" class="submit" type="submit"></button>
		<button title="Cancel" onclick="form_actions.cancelSubmission(this); return false;" class="cancel"></button>
	</form>

	<?php //Add Note form. No button to expose this. Instead it displays as part of the add file form ?>

	<form onsubmit='form_actions.submitForm(this, event);' method='post' id='add_note' action="<?=URL.'images/addNote'?>" class="add_note" name='add_note'>

		<h3 class="toggle_header_1"><a href="#file" class="add_file_toggle">Add Files</a> / Add Note</h3>
		<h3 class="toggle_header_2"><a href="#file" class="add_file_toggle">Add Files</a> / Edit Note</h3>

		<div id="note_syntax" class="note_syntax"></div>

		<label class="note_content">
			<span class="fullWidth">Note Content<a href="<?=URL.'images/getNoteSyntax'?>" id="note_syntax_toggle" class="note_syntax_toggle"><i> (show note syntax)</i></a></span>
			<textarea name='note_content' placeholder='Write…(in markdown or plain text)…' required></textarea>
		</label>

		<div class="note_tags">
		<label>
			<span class="fullWidth">Tags</span>
			<ul class="tag_entry">
				<li class="add_new_tag"><button type="button" title="Add New Tag" onclick="form_actions.addTag(this, '');">+</button></li>
			</ul>
		</label>
		</div>
		
		<button title="Submit" class="submit" type="submit"></button>
		<button title="Cancel" onclick="form_actions.cancelSubmission(this); notes.clearRestoreForm('#add_note', true); return false;" class="cancel"></button>

	</form>

	<?php //Move temp files form ?>

	<form onsubmit="form_actions.submitForm(this, event);" method="post" action="<?=URL.'images/moveTempFiles'?>" id="move_temp_files" class="move_temp_files" name="move_temp_files">
		<h3>Move Temp Files <sub>(please close form if you want to add more files)</sub></h3>
		<input name="imgArr[]" type="hidden" value="" required>

		<label>
			<span>Tags</span>
			<ul class="tag_entry">
				<li class="add_new_tag"><button type="button" title="Add New Tag" onclick="form_actions.addTag(this, '');">+</button></li>
			</ul>
		</label>		
		<button class="submit" type="submit"></button>
		<button onclick="form_actions.cancelSubmission(this); return false;" class="cancel"></button>
	</form>

	<?php //Edit multiple files form ?>

	<form onsubmit="form_actions.submitForm(this, event);" method="post" action="<?=URL.'images/editMulti'?>" id="edit_multi" class="edit_multi" name="edit_multi">
		<h3>Edit Multiple Tags <sub>(for selected images)</sub></h3>
		<label>
			<span>Remove current tags? <sub>Current tags will be removed and new tags added.</sub></span>
			<input type="checkbox" name="remove_tags" class="remove_tags">
		</label>
		<label>
			<span>Tags</span>
			<ul class="tag_entry">
				<li class="add_new_tag"><button type="button" title="Add New Tag" onclick="form_actions.addTag(this, '');">+</button></li>
			</ul>
		</label>	
		<button title="Submit" class="submit" type="submit"></button>
		<button title="Cancel" onclick="form_actions.cancelSubmission(this); return false;" class="cancel"></button>
	</form>

	<?php //Delete multiple files form ?>

	<form onsubmit="form_actions.submitForm(this, event);" method="post" action="<?=URL.'images/deleteImages'?>" id="delete_temp_files" class="delete_temp_files" name="delete_temp_files">
		<input name="imgArr[]" type="hidden">
		<input name="ID" type="hidden">
		<button title="Submit" class="submit" type="submit">Submit</button>
	</form>
</nav>