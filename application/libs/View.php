<?php
/**
* Class View
**/

class View{
	//renders the specified view
	public function render($filename, $render_without_header_and_footer = false){
		//page without header or footer
		if($render_without_header_and_footer == true){
			require VIEWS_PATH . $filename . '.php';
		}else{
			require VIEWS_PATH . '_templates/header.php';
			require VIEWS_PATH . $filename . '.php';
			require VIEWS_PATH . '_templates/footer.php';
		}
	}

	//feedback messages
	private function renderFooterContent($type){
		if($type === 'f'){
			$template = '_templates/feedback.php';
		}else if($type === 'm'){
			$template = '_templates/modal.php';
		}else if($type === 'l'){
			$template = '_templates/loading.php';
		}
		//show error/success messages(held in $_SESSION["feedback_postive/negative"])
		require VIEWS_PATH . $template;

		if($type === 'f'){
			//clear session messages after display
			Session::set('feedback_positive', null);
			Session::set('feedback_negative', null);
		}
	}

	private function footerScripts($navController){
		require VIEWS_PATH . $navController.'/_script.php';
	}

	//checks if passed string is currently actve controller
	private function checkForActiveController($filename, $navigation_controller){
		$split_filename = explode("/", $filename);
		$active_controller = $split_filename[0];

		if($active_controller == $navigation_controller){
			return true;
		}
		return false;
	}

	//Checks if the passed string is the currently active controller and controller-action.
	private function checkForActiveControllerAndAction($filename, $navigation_controller_and_action){
        $split_filename = explode("/", $filename);
        $active_controller = $split_filename[0];
        $active_action = $split_filename[1];

        $split_filename = explode("/", $navigation_controller_and_action);
        $navigation_controller = $split_filename[0];
        $navigation_action = $split_filename[1];

        if($active_controller == $navigation_controller && $active_action == $navigation_action) {
            return true;
        }
        // default return of not true
        return false;
    }

	private function pagination($totalCount){
		$paginationArr = []; //houses the generated pagination
		$url = $this->mode; //url for pagination links
		$count = ceil($totalCount / 20); //total count in lots of twenty
		$searchTerm = null;
		//include search term in links
		if(isset($this->search)){
			if(is_array($this->search)){
				$searchTerm = '/' . implode(',', $this->search);
			}else{
				$searchTerm = '/' . str_replace(' ', '_', $this->search);
			}
		}
		//set the start and end positions 
		$startPos = $this->pageNumber - ($this->pageNumber % 5);
		$endPos = $count < $startPos + 4 ? $count : $startPos + 4;
		$i = $startPos; //prefer to keep startPos as the same value throughout function
		//loop between start and end and push results into array
		for($i; $i <= $endPos; $i++){
			$link = URL.$url.'/'.$i.$searchTerm;
			if($i == $this->pageNumber){
				array_push($paginationArr, "<li class='activePage'><a href='$link'>$i</a></li>");
			}else if($i === 0){
				//don't do anything if zero
			}else{
				array_push($paginationArr, "<li><a href='$link'>$i</a></li>");
			}
		}
		if($this->pageNumber > 4){
			$prev = URL.$url.'/'.($startPos - 1).$searchTerm;
			$first = URL.$url.'/1'.$searchTerm;
			array_unshift($paginationArr, "<li><a href='$prev'>++</a></li>");
			array_unshift($paginationArr, "<li> <a href='$first' class='first_page'>First</a></li>");
		}

		if($count > 4 && $count > $endPos){
			$next = URL.$url.'/'.($endPos + 1 ).$searchTerm;
			$last = URL.$url.'/'.$count.$searchTerm;
			array_push($paginationArr, "<li><a href='$next'>++</a></li>");
			array_push($paginationArr, "<li> <a href='$last' class='last_page'>Last</a></li>");
		}

		array_unshift($paginationArr, '<ul class="pagination">');
		array_push($paginationArr, '</ul>');

		return $paginationArr;
	}

    private function search(){
    	//if search specified, convert string to array, loop through and get title from tags array
    	//add to string and print
    	if(!empty($this->search)){
    		$counter = 0;
    		if(is_array($this->search)){
    			$tagStr = "";
    			foreach($this->search as $search){
	    			$counter++;
	    			if(array_key_exists($search, $this->tags[2])){
	    				if($counter === 1){
	    					$tagStr .= $this->tags[2][$search]['title'];
	    				}else{
	    					$tagStr .= ', '.$this->tags[2][$search]['title'];
	    				}
	    			}
	    		}
    		}else{
    			$tagStr = $this->search;
    		}
    		
			echo "<h2 class='search_results'>Search Results: <sub>( ".$tagStr." )</sub></h2>";
		}
    }

    private function sidebar($type){
    	$this->media_type = $type;
    	require VIEWS_PATH . '_templates/_sidebar.php';
    }

    private function sidebarRevealBtn(){
    	echo '<button class="sidebarReveal" id="sidebarReveal" onclick="utils.sidebarRevealMobile(this);">Reveal Sidebar</button>';
    }

    private function no_content($title, $description){
    	echo "<div class='no_content'>";
    	echo "<h2>$title</h2>";
    	echo "<p>$description</p>";
    	echo "</div>";
    }
}