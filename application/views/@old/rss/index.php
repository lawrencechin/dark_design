<section class="rss_feeds">
<?php 
$this->sidebar('f');
$this->sidebarRevealBtn();
	if(!empty($this->feeds) && count($this->feeds) > 1){
		
		echo '<ul id="rss_feed_list" class="vertical_list rss_feed_list">';
		foreach($this->feeds as $feeds){ 
			if(isset($feeds->count)){/**ignore*/}else{?>
			<li>
				<sub>date: </sub><span><?=$feeds->PostDate?></span>
				<br>
				<sub>title: </sub><h2 class="light_heading"><a href="<?=$feeds->ID?>"><?=(strlen($feeds->Title) > 1) ? $feeds->Title : "No Title"?></a></h2>
				<br>
					
				<ul class="tag_entry">
				<sub>tags: </sub>
					<?php 
					$count = 0;
					if(!empty($feeds->tag_id)){
						$tags = explode(',', $feeds->tag_id);
						$tag_titles = explode(',', $feeds->tag_title);
						foreach($tags as $tags){
							echo '<li class="tag_list_item"><a href="'.URL.'rss/index/'.$tags.'" data-id="'.$tags.'">'.$tag_titles[$count].'</a></li>';
							$count++;
						}
					}?>
				</ul>		

				<br>
				<p class="description"><button data-info='{"title" : "<?=$feeds->Title?>", "url" : "<?=$feeds->Perma?>"}' title="Add to Bookmarks" onclick="form_actions.rssFormReveal(this);" class="add_bookmark"></button><?=$feeds->Description?></p>
			</li><?php } }
			if(isset($this->feeds['count']->count) && $this->feeds['count']->count > 50){
				$search = isset($this->search) && !empty($this->search) ? $this->search : '';
				echo '<button id="load_more" class="load_more" data-page="2" data-search="'.$search.'" onclick="rss.loadMore(this)">Load More</button>';
			}
			echo '</ul>';
			echo '<button title="Close Content" id="close_content" class="close_content" onclick="utils.rssContentHide(this, \'#rss_content\');">Close</button>';
			echo '<div id="rss_content" class="rss_content"></div>'; ?>
			<div id="add_book_form">
				<form onsubmit="form_actions.submitForm(this, event);" method="post" action="<?=URL.'bookmarks/create'?>" name="add_bookmark" class="add_bookmark">
					<h3>Add to Bookmarks</h3>

					<label>
						<span>Bookmark Title</span>
						<input name="book_title" type="text" placeholder="title…" value="">
					</label>

					<input type="hidden" name="book_url" value="">

					<input type="checkbox" name="book_rss" checked style="display: none;">
					
					<label>
					<span>Tags</span>
					<ul class="tag_entry">
						<li class="add_new_tag"><button type="button" title="Add New Tag" onclick="form_actions.addTag(this, '');">+</button></li>
					</ul>
					</label>

					<button title="Submit" class="submit" type="submit"></button>
					<button title="Cancel" onclick="form_actions.removeRssForm(this); return false;" class="cancel"></button>
				</form>
			</div>
	<?php }else{
		$this->no_content('All feeds read', 'Try refreshing? Or maybe you haven\'t added any feeds yet…');
	} 
?>
</section>