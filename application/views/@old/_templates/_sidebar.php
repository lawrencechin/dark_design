<?php
$searchbar = false;
$header = "Filter by Tag :";
$head = '<li class="no_bullet"><h3 class="light_heading">Search Tag List : </h3></li>';
$flags = false;

if($this->media_type == 'i'){
	$idType = "image_sidebar";
	$header = "View Images by Tag :";
	$link = URL.$this->mode.'/1/';
	$all = '<li class="tag_list_item"><a href="'.$link.'">all</a></li>';

}else if($this->media_type == 'b'){
	$idType = "bookmarks_sidebar";
	$link = URL.'bookmarks/index/';
	$all = '<li class="tag_list_item"><a href="'.$link.'">all</a></li>';
	$head = '<li class="no_bullet"><h3 class="light_heading">Search via: </h3></li>';
	$searchbar_opt_1 = "#bookmarks_list";
	$searchbar_opt_2 = ".filter_book";
	$searchbar ="<li class=\"no_bullet\"><input onfocus=\"navigation.side_search(this, '$searchbar_opt_1', '$searchbar_opt_2');\" onblur=\"navigation.unbind_side_search(this);\" type='text' placeholder='search bookmarks…' id='sidebar_search' class='sidebar_search search'></input></li>";
	$searchbar_btn = "<button class='light_tag' onclick='navigation.side_search_criteria(this);' data-info='{\"cont\" : \"#bookmarks_sidebar\", \"criteria\" : \"li:not(.no_bullet)\", \"place\" : \"search tags…\"}'>tags</button>";
	$searchbar_btn_2 = "<button class='light_book active' onclick='navigation.side_search_criteria(this);' data-info='{\"cont\" : \"$searchbar_opt_1\", \"criteria\" : \"$searchbar_opt_2\", \"place\" : \"search bookmarks…\"}'>bookmarks</button>";

}else{
	$idType = "rss_sidebar";
	$link = URL.'rss/index/';
	$all = '<li id="all_feeds" class="tag_list_item"><a href="'.$link.'">all</a></li>';
	$flag_day = array(1, 'a day');
	$flag_week = array(7, 'a week');
	$flag_month = array(31, 'a month');
	$flag_all = array(100, 'all');
	$mark_read = "<h3 class='light_heading'>Mark articles as read that are older than : </h3>";
	$flag_button = function($flag, $title){
		echo "<button class='mk_read' onclick='rss.mark_as_read(this);' data-flag='$flag'>$title</button>";
	};

	$flags = true;
} 

$searchbar_opt_2 = "li:not(.no_bullet)";
if(!$searchbar){
	$searchbar ="<li class=\"no_bullet\"><input onfocus=\"navigation.side_search(this, '#$idType', '$searchbar_opt_2');\" onblur=\"navigation.unbind_side_search(this);\" type='text' placeholder='search tags…' id='sidebar_search' class='sidebar_search search'></input></li>";
}
?>
<ul id="<?=$idType?>" class="sidebar">
<?php 
echo '<button class="close_sidebar" onclick="utils.sidebarHideMobile(this);" id="close_sidebar"></button>';
if($flags){
	echo $mark_read;
	$flag_button($flag_day[0], $flag_day[1]);
	$flag_button($flag_week[0], $flag_week[1]);
	$flag_button($flag_month[0], $flag_month[1]);
	$flag_button($flag_all[0], $flag_all[1]);
}

echo !empty($head) ? $head : "";
echo !empty($searchbar_btn) ? $searchbar_btn_2.$searchbar_btn: "";
echo $searchbar;
//selected tags
if($this->tags[0]){
	echo '<li class="no_bullet">';
	echo '<ul class="selected_tags tag_entry">';
	echo '<li class="no_bullet"><h3 class="light_heading">Selected Tags</li>';
	foreach($this->tags[0] as $key => $value){ 
		$selected_link = !empty($link) ? $link.$this->search : "#";
		$removeLink = $link;
		foreach(explode(',', $this->search) as $key => $search){
			if($search !== $value['id']){
				$removeLink .= $search . ',';
			}
		}
		$removeLink = substr($removeLink, 0, -1);
		echo '<li class="tag_list_item"><a class="selected_tag" href="'.$selected_link.'">'.$value['title'].'<sub>('.$value['relatedCount'].')</sub></a></li><a href="'.$removeLink.'" class="tag_delete"><span class="removeFav"></span></a>';	
	}
	echo '</ul>';
}else{
	echo '<li class="no_bullet"><h3 class="light_heading">'.$header.'</h3>';
}


//related tags
if($this->tags[1]){
	echo '<ul class="related_tags tag_entry">';
	echo '<li class="no_bullet"><h3 class="light_heading">Related Tags</li>';
	foreach($this->tags[1] as $key => $value){ 
		$related_link = !empty($link) ? $link.$this->search.','.$key : "#";
		echo '<li class="tag_list_item"><a class="related_tag" href="'.$related_link.'">'.$value['title'].'<sub>('.$value['relatedCount'].')</sub></a></li>';
	}
	echo '</ul>';
}


//the rest
echo '<ul class="other_tags tag_entry">';
if($this->tags[0]){
	echo '<li class="no_bullet"><h3 class="light_heading">Other Tags</li>';
}
echo !empty($all) ? $all : "";
echo '<li class="tag_list_item"><a href="'.$link.'tl">untagged</a></li>';
if($this->tags[2]){
	foreach($this->tags[2] as $key => $value){ 
		$tag_link = !empty($link) ? $link.$key : "#";
		echo '<li class="tag_list_item"><a href="'.$tag_link.'">'.$value['title'].'<sub>('.$value['count'].')</sub></a></li>';
		
	}	
}
echo '</ul>'; ?>
</ul>