var utils = {
	loading : $('#loading_and_thumbs'),
	snp : $('#shallNotPass'),
	//switch when uploading
	url : 'http://captainlc.000space.com/darkdesign/',
	urlLOCAL : 'http://darkdesign.local/',
	mediaQuery : window.matchMedia( "(max-width : 48.75em)" ),

	sidebarRevealMobile: function(){
		$('.sidebar').toggleClass('sidebar_reveal');
	},

	sidebarEnable: function(){
		if(utils.mediaQuery.matches){
			if($('.sidebar').length){
				$('#sidebarReveal').prop('disabled', false);
			}
		}
	},

	//in safari, once the viewport has been scrolled you cannot scroll the fixed position sidebar - I can't find a reason for this.
	sidebarFocus : function(){
		$('.sidebar').on({
			'mouseover' : function(){
			$(this).addClass('fixedPosFix');
			},

			'mouseleave' : function(){
				$(this).removeClass('fixedPosFix');
			}
		});
	},

	rssTweakHeader : function(){
		//at the smallest size supported the secondary header collapses - we can fix this by truncating the text without changing the meaning
		if(utils.mediaQuery.matches){
			var $truncateElems = document.getElementById('header').getElementsByClassName('secondary_header')[0].getElementsByTagName('li');
			$truncateElems[1].getElementsByTagName('a')[0].innerText = "View";
			$truncateElems[2].getElementsByTagName('a')[0].innerText = "Edit";
		}
	},

	imagesTweakHeader : function(){
		//at the smallest size supported the secondary header collapses - we can fix this by truncating the text without changing the meaning
		if(utils.mediaQuery.matches){
			var $truncateElems = document.getElementById('header').getElementsByClassName('secondary_header')[0].getElementsByTagName('li');
			$truncateElems[1].getElementsByTagName('a')[0].innerText = "Temp";
		}
	},

	rssContentReveal: function(cont){
		if(utils.mediaQuery.matches){
			cont.addClass('sidebar_reveal');
			$('#close_content').show();
		}
		
	},

	rssContentHide: function(btn, cont){
		if(utils.mediaQuery.matches){
			$(cont).removeClass('sidebar_reveal'); 
			$(btn).hide();
		}
		
	},

	keyBoardControls: function(){
		Mousetrap(document.getElementById('search')).bind('enter', function(e){
			if($('#search').val().length){
				$('#search').siblings('#submit_search').trigger('click');
			}
		});

		Mousetrap.bind('left', function(){
			if($('#previous').is(':visible')){
				$('#previous').find('button').trigger('click');
			}
		});
		Mousetrap.bind('right', function(){
			if($('#next').is(':visible')){
				$('#next').find('button').trigger('click');
			}
		});
		Mousetrap.bind('shift+left', function(){
			if($('#origSize').is(':visible')){
				$('#origSize').find('button').trigger('click');
			}
		});
		Mousetrap.bind('shift+right', function(){
			if($('#conWidth').is(':visible')){
				$('#conWidth').find('button').trigger('click');
			}
		});
		Mousetrap.bind('up', function(){
			if($('#conHeight').is(':visible')){
				$('#conHeight').find('button').trigger('click');
			}
		});
		Mousetrap.bind('x', function(){
			if($('#closeModal').is(':visible')){
				$('#closeModal').find('button').trigger('click');
			}
		});
	},

	pleaseWait : function(){
		utils.loading.fadeIn();
		utils.snp.fadeIn().addClass('loading');
	},

	simpleSuccess : function(){
		utils.loading.fadeOut();
		setTimeout(function(){
			utils.loading.removeClass('loading');
		}, 400);
	},

	success : function(){
		//removed tada animation, performance was effected
		utils.snp.removeClass('loading').addClass('success');
		utils.snp.find('p').html('Success!');
	},

	successRemove : function(){
		utils.loading.fadeOut();
		utils.snp.removeClass('success').fadeOut();
		utils.snp.find('p').html('Please Wait');
	},

	error : function(msg){
		//removed wobble animation, performance was effected
		utils.snp.removeClass('loading').addClass('error');
		utils.snp.find('p').html(msg);
		
		setTimeout(function(){
			utils.loading.fadeOut();
			utils.snp.removeClass('error').fadeOut();
			utils.snp.find('p').html('Please Wait');
		}, 2000);
	} 
	
};

var navigation = {
	//used to grab either image or video and stick them in a model window
	currentElem : null,
	activeSidebar : null,

	load : function(cont){
		$(cont).on('click', 'li:not(.tag_list_item) > a:not(.change_colour)', function(e){
			e.preventDefault();
			var $this = $(this),
			$link = $this.attr('href'),
			$modal = $('#modal'),
			$div = $modal.find('div');

			if($this.parents('li').hasClass('notes_md')){
				window.open($link);
			}else{
				navigation.currentElem = $this;

				$div.hide().find('img').empty();

				$div.show().find('img').attr('src', $link);
				$modal.fadeIn();
				document.body.style.overflow = 'hidden';
			}
		});
	},

	//hide feedback
	hide_feedback : function(btn){
		$(btn).parents('.feedback').hide();
	},

	//search main window or sidebar list
	side_search_criteria : function(btn){
		var $btn = $(btn),
		$jsonObj = JSON.parse($btn.attr('data-info')),
		$cont = $jsonObj.cont,
		$criteria = $jsonObj.criteria,
		$placeholder = $jsonObj.place,
		$sidebar = $('#sidebar_search');

		$btn.siblings('button').removeClass('active').end().addClass('active');

		$sidebar.val('');
		$('#bookmarks_list').find('.filter_book').show();
		$('#bookmarks_sidebar').find('li').show();
		navigation.unbind_side_search($sidebar);

		$sidebar.attr('onfocus', 'navigation.side_search(this, \''+$cont+'\',\''+$criteria+'\');');
		$sidebar.attr('placeholder', $placeholder);
	},

	//sidebar search
	side_search : function(searchbar, cont, filterElem){
		$(cont).liveFilter($(searchbar), filterElem);
	},

	//remove sidebar search
	unbind_side_search : function(sidebar){
		$(sidebar).unbind('keyup');
	},

	previous : function(){
		var prevElem = this.currentElem.parent().prev();
		if(prevElem.length > 0){
			prevElem.find('a').first().trigger('click');
		}else{
			this.currentElem.parent().parent().find('li > a').last().trigger('click');
		}
	},

	next : function(){
		var nextElem = this.currentElem.parent().next();
		if(nextElem.length > 0){
			nextElem.find('a').first().trigger('click');
		}else{
			this.currentElem.parent().parent().find('li > a').first().trigger('click');
		}
	},

	//Refer to previous versions for img resize functionality
	imgResize : function(){
		var $modal = document.getElementById('modal');
		if($modal){
			var img = $modal.getElementsByTagName('img')[0];
			var natHeight = img.naturalHeight;
			img.addEventListener('mouseenter', function(){
				img.style.cursor = navigation.imgResizeConditions(img)[0];
			});
			img.addEventListener('mousedown', function(){
				var options = navigation.imgResizeConditions(img);
				img.style.height = options[1];
				img.style.cursor = options[2];
			});
		}
	},

	imgResizeConditions : function(img){
		var natHeight = img.naturalHeight;
		var currentHeight = img.clientHeight;
		var clientHeight = img.parentElement.clientHeight;
		if(natHeight > clientHeight){
			if(currentHeight === clientHeight){
				return ['zoom-in', 'auto', 'zoom-out'];
			}else{
				return ['zoom-out', '100%', 'zoom-in'];
			}
		}else{
			if(currentHeight === clientHeight){
				return ['zoom-out', 'auto', 'zoom-in'];
			}else{
				return ['zoom-in', '100%', 'zoom-out'];
			}
		}
	},

	closeModal : function(){
		$('#modal').find('div').hide().find('img').empty().end().end().find('section').remove().end().fadeOut();
		document.body.removeAttribute('style');
	},

	preview : function(btn){
		var $btn = $(btn),
		$id = $btn.attr('data-id'),
		$url = utils.url + 'images/getFirstImg/' + $id;

		$.ajax({
			url : $url,
			dataType : 'json'
		}).success(function(data){
			var $img;
			if(!data.img_address){
				alert('No image to preview');
			}else{
				$img = $('<a class="img_preview_link" href="'+utils.url+'images/getImages/1/'+$id+'"><img class="preview_img" src="'+data.img_address+'"></a>');
				var $close_btn = $('<button onclick="navigation.close_preview(this);" class="delete_popup" title="Remove Preview"></button>');

				$btn.parents('li').append($img, $close_btn);
			}
		}).error(function(){
			alert('No image to preview');
		});
	},

	close_preview : function(btn){
		$(btn).siblings('.img_preview_link').fadeOut().end().fadeOut();

		setTimeout(function(){
			$(btn).siblings('.img_preview_link').remove().end().remove();
		}, 700);
	},

	toggle_book_note : function(btn){
		if(btn.id == 'book_toggle'){
			document.forms['add_note'].style.display = 'none';
			document.forms['add_bookmark'].style.display = 'block';
			document.getElementById('book_toggle').classList.add('active_toggle');
			document.getElementById('note_toggle').classList.remove('active_toggle');
		}else{
			document.forms['add_note'].style.display = 'block';
			document.forms['add_bookmark'].style.display = 'none';
			document.getElementById('book_toggle').classList.remove('active_toggle');
			document.getElementById('note_toggle').classList.add('active_toggle');
		}
	}
};

var form_actions = {
	//holds the active form used for post-submission actions
	activeForm : null,
	checked_images : null,
	edit_note : null,
	delayTimeout : null,
	deleteMultiBook : false,
	//reveal forms
	revealForm : function(btn){
		//function attached to buttons onclick
		var $btn = $(btn),
		$formClass = $btn.attr('class'),
		form = document.forms[$formClass];

		$(form).fadeToggle().find('.add_new_tag').children().prop('disabled', false);
		//add all tags for selected items in order to remove
		if($(form).attr('ID') == 'edit_multi'){
			this.buildEditForm(form);
		}

		if(form.parentNode.id == 'bookmark_form'){
			if(document.getElementById('bookmark_form').style.display == 'block'){
				form_actions.removeModalForm();
			}else{
				document.getElementById('bookmark_form').style.display = "block";
				$(document.forms['add_note']).find('.add_new_tag').children().prop('disabled', false);
			}
		}
	},

	buildEditForm : function(form){
		//grab the tags for checked elems and populate the form with results
		var checkedImages = document.getElementsByTagName('section')[0].getElementsByClassName('checked_image');
		var tagArr = [];
		//empty tags if present
		$.each($(form).find('.tag_entry .tag_removal').children(), function(){
			this.remove();
		});
		//loop through tag elements and save data to array
		for(var i = 0; i < checkedImages.length; i++){
			var $tags = checkedImages[i].parentNode.parentNode.getElementsByClassName('tag_entry')[0].getElementsByClassName('tag_list_item');
			for(var j = 0; j < $tags.length; j++){
				var $value = $tags[j].firstChild.innerText;
				var $href = $tags[j].firstChild.href;
				//the tag id is part of the url/href
				var $id = $href.substr(($href.lastIndexOf('/') + 1));
				tagArr[$id] = $value;
			}
		}

		//build nodes to insert into form(nb. written in plain js for my own benefit)
		for(var key in tagArr){
			var $li = document.createElement('li'),
			$a = document.createElement('a'),
			$span = document.createElement('span'),
			$a_delete = document.createElement('a'),
			$tagText = document.createTextNode(tagArr[key]);

			$a.appendChild($tagText);
			$a.href = '#';
			$a.setAttribute('data-id', key);
			$li.classList.add('tag_list_item');
			$li.classList.add('stasis_tag');
			$li.appendChild($a);

			$a_delete.href = '#';
			$a_delete.classList.add('tag_delete');
			$a_delete.classList.add('st_tag');
			$span.classList.add('removeFav');
			$a_delete.appendChild($span);

			form.getElementsByClassName('tag_removal')[0].appendChild($li);
			form.getElementsByClassName('tag_removal')[0].appendChild($a_delete);
		}
	},

	//inline editing, toggling edit btn removes inputs and restores data 
	revealFormInline : function(btn){
		var $btn = $(btn),
		$container = $btn.parents('li'),
		$editable_items = $container.find('.editable');
		$btn.toggleClass('activeBtn');

		if($btn.hasClass('activeBtn')){
			$container.find('input').fadeIn();
			$container.find('.hidden_label').fadeIn();
			$editable_items.hide();

			$container.find('.tag_entry > .tag_delete').fadeIn();
			$container.find('.tag_entry > .tag_delete_new').fadeIn();
			$container.find('.add_new_tag').css('display', 'inline-block').children().prop('disabled', false);
			var $note_colour = $container.find('.note_colour');
			if($note_colour.length > 0){
				$note_colour[0].style.display = 'inline-block';
			}
		}else{
			//fadeout inputs and remove from DOM
			$container.find('input').hide().autocomplete('dispose');
			$container.find('.editable').show();
			$container.find('.tag_entry > .tag_delete').hide();
			$container.find('.tag_entry > .tag_delete_new').hide();
			$container.find('.add_new_tag').hide();
			$container.find('.hidden_label').hide();
			var $note_colour = $container.find('.note_colour');
			if($note_colour.length > 0){
				$note_colour[0].style.display = null;
			}
			setTimeout(function(){
				$container.find('.new_tag').remove();
			}, 600);
		}
	},

	rssFormReveal : function(btn){
		var $btn = $(btn),
		$data = JSON.parse($btn.attr('data-info')),
		$formCont = $('#bookmark_form');
		document.forms['add_bookmark'].style.display = 'block';
		$formCont.find('input[name="book_title"]').val($data.title);
		$formCont.find('input[name="book_url"]').val($data.url);
		$formCont.fadeIn();
	},

	removeModalForm : function(){
		var $bkForm = document.getElementById('bookmark_form');
		var $forms = $bkForm.getElementsByTagName('form');
		$bkForm.style.display = 'none';
		for(var i = 0; i < $forms.length; i++){
			$forms[i].style.display = 'none';
		}
		if(document.getElementById('note_syntax').style.display == 'block'){
			document.getElementById('note_syntax_toggle').click();
		}
		document.getElementById('book_toggle').classList.add('active_toggle');
		document.getElementById('note_toggle').classList.remove('active_toggle');
	},	

	addTag : function(btn, postURL){
		var $btn = $(btn);
		//disable so only one input is added
		$btn.prop('disabled', true);

		if(postURL === ''){
			$input = "<input class='new_tag' name='tag' type='text' placeholder='separate tags with commas' data-id='' data-postURL='' onfocus='search.tags(this, false); form_actions.delay();' autocomplete='off'>";
		}else{
			//add autocomplete here for each added input
			if($(btn).data('mediatype') == 'n'){
				$input = "<input class='new_tag' name='tag' type='text' placeholder='separate tags with commas' data-id='"+$(btn).attr('data-id')+"' data-postURL='"+postURL+"' data-mediatype='n' onfocus='search.tags(this, false); form_actions.delay();' autocomplete='off'>";
			}else{
				$input = "<input class='new_tag' name='tag' type='text' placeholder='separate tags with commas' data-id='"+$(btn).attr('data-id')+"' data-postURL='"+postURL+"' onfocus='search.tags(this, false); form_actions.delay();' autocomplete='off'>";
			}
		}
		
		$($input).insertBefore($btn.parent()).focus();
		this.tagCommaCheck($btn.parent().prev());
	},

	tagCommaCheck : function($input){
		$input.on('keyup', function(e){
			//test for comma(,)
			if(/(188)/.test(e.which)){
			//get value of text and separate out word upto comma
		    	var $text = $(this).val(),
		    	$commaPos = $text.indexOf(',');

		    	if($commaPos > 1){
		    		var $tag = $text.slice(0, $commaPos).replace(/(^\s*)|(\s*$)/gi, "").replace(/[ ]{2,}/gi," ").toLowerCase();
		    		var $newLi = "<li class='tag_list_item stasis_tag'><a href='#'>"+$tag+"</a></li><a href='#' class='tag_delete st_tag'><span class='removeFav'></span></a>";
			       	$($newLi).insertBefore($(this).val(""));
			       	var $new_tag = $(this).prev().prev();
			       	form_actions.submitTag($input, $new_tag);
		    	}
			} 
		});
	},

	removeTag : function(container, url_1, url_2){
		$(container).on('click', '.tag_delete', function(e){
			e.preventDefault();
			var $this = $(this),
			$url = $this.attr('href');
			$.ajax({
				url : $url,
				dataType : 'json'
			}).success(function(data){
				if(!data.error){
					$this.fadeOut().prev('li').fadeOut();
					setTimeout(function(){
						$this.prev('li').remove();
						$this.remove();
					}, 600);
				}else{
					utils.pleaseWait();
					utils.error('Could not remove tag, sorry');
				}
				
			}).error(function(){
				utils.pleaseWait();
					utils.error('Could not remove tag, sorry');
			});
		});
	},

	submitTag : function($input, $new_tag){
		var $tag_title = $new_tag.text(),
		$post_url = $input.attr('data-posturl'),
		$media_id = $input.attr('data-id'),
		$media_type = $input.attr('data-mediatype'),
		$formData = new FormData();

		if($tag_title.length){
			if($media_id.length > 0){
				$formData.append('tag_title', $tag_title);
				$formData.append('media_id', $media_id);
				if($media_type != null){$formData.append('media_type', $media_type);}

				$.ajax({
					url : $post_url,
					data : $formData,
					dataType: 'json',
					type: 'POST',
					processData : false,
					contentType : false
				})

				.done(function(resp){
					form_actions.tagAdded($new_tag, $input, $media_id, resp);
				})

				.error(function(resp){
					utils.pleaseWait();
					utils.error('Whoops! Not sure what happened. Won\'t happen again, promise.');
				});
			}else{
				$new_tag.next().css('display', 'inline-block').removeClass('st_tag');
			}
		}
	},

	delay : function(){
		if(form_actions.delayTimeout !== null){
			clearTimeout(form_actions.delayTimeout);
			form_actions.delayTimeout = null;
		}
	},

	tagAdded : function($tagElem, $input, $media_id, data){
		var $tagRemoveURL = data.media_type == 'n' ? utils.url + data.url_2 + data.tag_id + '/' + $media_id + '/' + data.media_type : utils.url + data.url_2 +data.tag_id + '/' + $media_id,
		$tagURL = utils.url + data.url_1 + data.tag_id;

		if(!data.error){
			$tagElem.removeClass('stasis_tag');
			$tagElem.children('a').attr('href', $tagURL);
			$tagElem.next().css('display', 'inline-block').removeClass('st_tag').attr('href', $tagRemoveURL);
		}else{
			$tagElem.next().remove().end().remove();
		}
		
	},
	//similar to the above, remove tags within form
	form_remove_tag : function(header){
		$(header).on('click', '.tag_delete', function(e){
			e.preventDefault();
			var $this = $(this);
			//form remove tags operates differently - we toggle state instead rather than fully removing them
			if($this.parents('ul').hasClass('tag_removal')){
				if($this.hasClass('re_add_tag')){
					$this.removeClass('re_add_tag');
					$this.prev().removeClass('lower_opacity');
				}else{
					$this.addClass('re_add_tag');
					$this.prev().addClass('lower_opacity');
				}
			}else{
				$this.prev().fadeOut().end().fadeOut();
				setTimeout(function(){
					$this.prev().remove();
					$this.remove();
				}, 100);	
			}
		});
	},
	//switches between states for tag removal within forms
	tag_toggle : function(check){
		//get elements next to checkbox(ul)
		var $container = check.nextElementSibling.children;
		if(check.checked){
			for(var i = 0; i < $container.length; i++){
				if($container[i].classList.contains('stasis_tag')){
					$container[i].classList.remove('stasis_tag');
				}else if($container[i].classList.contains('st_tag')){
					$container[i].classList.remove('st_tag');
					$container[i].setAttribute('style', 'display: inline-block');
				}
			}
		}else{
			for(var j = 0; j < $container.length; j++){
				if($container[j].classList.contains('tag_list_item')){
					$container[j].classList.add('stasis_tag');
				}else if($container[j].classList.contains('tag_delete')){
					$container[j].classList.add('st_tag');
					$container[j].removeAttribute('style', 'display: inline-block');
				}
			}
		}
	},

	submitEdit : function(input){
		var $input = $(input),
		$title = $input.val(),
		$id = $input.data('id'),
		$url = $input.val(),
		$postURL = $input.data('posturl'),
		$inputName = $input.attr('name'),
		$text = $input.siblings('h2').find('a').text() || $input.siblings('h3').text(),
		$href = $input.siblings('h2').find('a').attr('href'),
		$formData = new FormData();

		$input.addClass('activeInput');

		if($inputName == 'name'){
			$formData.append('book_name', $title);
			$formData.append('book_id', $id);
		}else if($inputName == 'url'){
			$formData.append('book_url', $url);
			$formData.append('book_id', $id);
		}

		if($text == $title || $title == $href){/**no change*/}else{
			form_actions.ajaxSend($postURL, $formData);
		}
	},

	deleteFormInline : function(btn){
		var $btn = $(btn),
		$parent = $btn.parents('li'),
		$formClass = $btn.attr('class'),
		$form = $parent.find('form.'+$formClass);

		if(confirm('Are you sure?')){
			$form.find('button[type="submit"]').trigger('click');
		}
	},

	cancelSubmission : function(btn){
		var $btn = $(btn);
		//check if form is inline or a header form
		if(!$btn.parents('ul').find('.activeBtn').trigger('click').length){
			var $form = $btn.parents('form');
			$form.fadeToggle().get(0).reset();

			if($form[0].classList.contains('add_bookmark') || $form[0].classList.contains('add_note')){
				form_actions.removeModalForm();
			}
			if($form.find('input[name="imgArr[]"]')){
				$form.find('input[name="imgArr[]"]').val("");
			}

			$form.find('.tag_entry').children().each(function(){
				if($(this).hasClass('new_tag')){
					$(this).autocomplete('dispose');
				}
				if(!$(this).hasClass('add_new_tag')){
					$(this).remove();
				}
			});
		}
	},

	select_image : function(btn, cont){
		$(btn).toggleClass('checked_image');
		form_actions.checked_images = $(cont + ' .checked_image');
		var $edit_multi_btn = $('.edit_multi'),
		$delete_files = $('.delete_temp_files');

		if(form_actions.checked_images.length > 0){
			$edit_multi_btn.prop('disabled', false);
			$delete_files.prop('disabled', false);

		}else{
			$edit_multi_btn.prop('disabled', true);
			$delete_files.prop('disabled', true);
		}
	},

	checkAll : function(btn, cont){
		var $btn = $(btn),
		$cont = $(cont),
		$checked = $cont.find('.checked_image');

		if($checked.length){
			$.each($checked, function(){
				$(this).trigger('click');
			});

			$btn.removeClass('checked_image');
		}else{
			$.each($cont, function(){
				$(this).find('.check_image').trigger('click');
			});

			$btn.addClass('checked_image');
		}
	},

	bookmark_delete_multi : function(btn){
		if(confirm('Are you sure you want to delete these bookmarks?')){
			//grab all the IDs from checked_images
			if(form_actions.checked_images.length > 0){
				var book_ids = [];
				form_actions.checked_images.each(function(){
					book_ids.push(this.getAttribute('data-id'));
				});
				var $form = new FormData();
				$form.append('book_ids', book_ids);
				var $action = utils.url + 'bookmarks/delete';
				form_actions.deleteMultiBook = false;
				form_actions.ajaxSend($action, $form);
			}
		}
	},

	submitForm : function(form, e){
		e.preventDefault();
		var $form = $(form),
		$action = $form.attr('action'),
		$valid = $form[0].checkValidity() ? true : false,
		$tagStr = '',
		$tag_remove_str = '',
		$tag_list = $form.find('.tag_entry').not('.tag_removal'),
		$data = new FormData($form.get(0));
		this.activeForm = $form,
		$note_colour = false;
		//check if the form being submitted is add note
		if($form[0].id == 'add_note'){
			//get the colour class name
			document.getElementById('bookmark_form').className.split(/\s+/).filter(function(item, index, arr){
				if(item.indexOf('note_') > -1){
					$note_colour = arr[index];
				}
			});
			//if nothing was returned then set note_colour to 'note_black'
			$note_colour = $note_colour || 'note_black';
		}

		if($note_colour){
			$data.append('note_colour', $note_colour);
		}
		//validate form using html5, return false if invalid
		if(!$valid){ return false; }

		//loop through tags ul and get text data, convert into csv and latterly append to form obj
		if($tag_list){
			$.each($tag_list.find('li'), function(){
				if($(this).hasClass('tag_list_item')){
					$tagStr += $(this).text() + ',';
				}
			});

			if($tagStr.length > 0){
				$tagStr = $tagStr.slice(0, -1);
			}

			$data.append('tags', $tagStr);
		}

		if($form.attr('ID') == 'edit_multi'){
			var $checked_images = [];
			if($form[0].getElementsByClassName('remove_tags')[0].checked){
				var $tag_remove_list = $form[0].getElementsByClassName('tag_removal')[0].children;
				for(var i = 0; i < $tag_remove_list.length; i++){
					if($tag_remove_list[i].classList.contains('tag_list_item') && ! $tag_remove_list[i].classList.contains('lower_opacity')){
						var $tag_id = $tag_remove_list[i].children[0].getAttribute('data-id');
						$tag_remove_str += $tag_id + ',';
					}
				}

				if($tag_remove_str.length > 0){
					$tag_remove_str = $tag_remove_str.slice(0, -1);
				}

				$data.append('tag_removal', $tag_remove_str);
			}

			form_actions.checked_images.each(function(){
				$checked_images.push($(this).attr('data-id'));
			});
			$data.append('ID', $checked_images);
		}
		//and finally…submit!
		form_actions.ajaxSend($action, $data);
	},

	ajaxSend : function(action, data){
		utils.pleaseWait();

		$.ajax({
			url : action,
			data : data,
			type : 'POST',
			dataType : 'json',
			processData: false,
			contentType: false
		})

		.done(function(resp){
			form_actions.ajaxResult(resp);
		})

		.error(function(resp){
			utils.error('Whoops! Not sure what happened. Won\'t happen again, promise.');
		});
	},

	ajaxResult : function($json){
		//classes loading, success tada, error wobble
		if($json.error === false){
			//single delete form actions
			if($json.msg == 'Tag_Added'){
				ajaxResponses.tagAdded($json);
			}else if($json.msg == 'Title_Updated' || $json.msg == 'URL_Updated'){
				ajaxResponses.editComplete($json);
			}else if($json.msg == 'Bookmark_Deleted' || $json.msg == 'feed_deleted' || $json.msg == 'note_deleted' || $json.msg == 'Bookmark_Deleted_Multi'){
				ajaxResponses.bookDeleted();
			//actions for temp files and multiple DOM elems
			}else if($json.msg == 'Bookmark_Added'){
				ajaxResponses.bookmarkAdded();
			}else if($json.msg == 'Tags_Updated'){
				ajaxResponses.reload();
			}else if($json.msg == 'note_updated'){
				ajaxResponses.noteUpdated($json);
			}else if($json.msg == 'note_not_updated'){
				ajaxResponses.noteNotUpdated();
			}else if($json.msg == 'RSS_Bookmark_Added'){
				ajaxResponses.rssBook();
			}else if($json.msg == 'Temp_Image_Deleted'){
				ajaxResponses.tempDeleted();
			//actions for when images have been uploaded
			}else if($json.msg == 'Image_Added'){
				ajaxResponses.imageAdded($json);
			//actions for red	
			}else if($json.msg == 'Tag_Deleted'){
				ajaxResponses.tagDeleted();
			}else if($json.msg == 'Tag_Edited'){
				ajaxResponses.tagEdited();
			}else if($json.msg == 'Marked' || $json.msg == 'Feed_Added'){
				ajaxResponses.reload();
			}else if($json.msg == 'note_added'){
				utils.simpleSuccess();
				document.forms['add_note'].getElementsByClassName('cancel')[0].click();
			}else{
				ajaxResponses.defaultFormResp();
			}
		}else{
			if($json.msg == 'Title_NOT_Updated' || $json.msg == 'URL_NOT_Updated' || $json.msg == 'Tag_Not_Added'){
				//inline editing - if no data is sent through then nothing should happen
				utils.simpleSuccess();
			}else if($json.msg == 'Tag_Not_Updated'){
				ajaxResponses.tagNotAdded();
			}else if($json.msg == 'content_offline'){
				utils.error('Content offline. Upload/Delete functions disabled');
			}else if($json.msg == 'Disabled in demo'){
				utils.error($json.msg);
			}else{
				utils.error('There has been a general error. Sorry.');
			}
			
			setTimeout(function(){
				if($json.msg == 'Tag_Undeleted'){
					search.tagDelete.parent().removeClass('activeEdit');
					search.tagDelete.replaceWith($('<a class="tag_edit" href="#">'+search.tagValue+'</a>'));
					form_actions.activeForm = null;
				}else{
					form_actions.activeForm = null;
				}
			}, 1500);
		}
	}
};

var rss = {
	//click article to load content and mark item as read
	load_content : function(container){
		$(container).on('click', ' h2 > a', function(e){
			e.preventDefault();
			var $this = $(this);
			var $id = $this.attr('href');
			var $parent = $this.parents('li');
			var $rss_content = $('#rss_content');

			if(!$parent.hasClass('dimmed_article')){
				$id = $id + '/1';
			}

			$rss_content.load(utils.url + 'rss/getContent/' + $id, function(){
				utils.rssContentReveal($rss_content);
				if(!$parent.hasClass('dimmed_article')){
					$parent.addClass('dimmed_article');
					//grab any tags present and then deincrement sidebar totals
					$parent.find('.tag_list_item').each(function(){
						var $id = this.children[0].getAttribute('data-id');
						var $related_id = 'r' + $id;
						var $selected_id = 's' + $id;
						var $count_1 = document.getElementById($id);
						var $count_2 = document.getElementById($related_id);
						var $count_3 = document.getElementById($selected_id);

						if($count_1){
							$count_1.innerText = '(' + (parseInt($count_1.innerText.slice(1,-1)) -1) + ')';
						}

						if($count_2){
							$count_2.innerText = '(' + (parseInt($count_2.innerText.slice(1,-1)) -1) + ')';
						}

						if($count_3){
							$count_3.innerText = '(' + (parseInt($count_3.innerText.slice(1,-1)) -1) + ')';
						}

					});
				}
				$(this).get(0).scrollTop = 0;
			});
		});
	},

	//refresh feeds
	refresh_feeds : function(){
		//get last item ID and feed count
		var $rss_list = $('#rss_feed_list');

		utils.pleaseWait();

		$.ajax({
			url : utils.url + 'rss/refreshFeeds/',
			type : 'GET',
			dataType : 'json'
		})

		.done(function(resp){
			rss.response(resp);
		})

		.error(function(resp){
			utils.error(resp.msg);
		});
	},

	response : function(resp){
		if(!resp.error){
			utils.success();
			setTimeout(function(){
				utils.successRemove();
				document.location.href = utils.url+'rss/index';
			}, 800);
			
		}else{
			utils.error(resp.msg);
		}
	},

	//mark as read(day | week | month | all)
	mark_as_read : function(btn){
		var $btn = $(btn),
		$msg = $btn.text() == 'all' ? 'Mark all feeds as read?' : 'Mark feeds older than '+$btn.text()+' as read?',
		$flag = $btn.attr('data-flag'),
		$confirm = confirm($msg);

		if($confirm){
			utils.pleaseWait();
			$.ajax({
				url : utils.url + 'rss/markAsRead/' + $flag + '/',
				type : 'GET',
				dataType : 'json'
			})
			.done(function(resp){
				if(!resp.error){
					form_actions.ajaxResult(resp);
				}else{
					utils.error(resp.msg);
				}
			})
			.error(function(resp){
				utils.error(resp.msg);
			});
		}
		
	},

	content : function(){
		$('#rss_content').on('click', 'a', function(e){
			e.preventDefault();
			window.open($(this).attr('href'));
		});
	},

	loadMore : function(btn){
		var $btn = $(btn),
		$page = $btn.attr('data-page'),
		$search = $btn.attr('data-search'),
		$cont = $('#rss_feed_list'),
		$addBtn = $('#load_more');

		$.get(utils.url+'rss/loadMore/'+$page+'/'+$search, function(data){
			$addBtn.remove();
			$cont.append(data);
		});
	}
};

var temp = {

	tempContent : null,
	imgArr : null,

	selectImages : function(){
		var $cont = $('#temp_files'),
		$tempViewer = $('#temp_files_viewer'),
		$editBtn = $('.move_temp_files'),
		$deleteBtn = $('.delete_temp_files'),
		$bool = true;

		if($tempViewer.is(':visible')){
			$bool = false;
		}

		$cont.on('click', 'a', function(e){
			e.preventDefault();
			var $this = $(this),
			$img = $('<img class="temp_image" src=""></img>');

			$this.toggleClass('activeImageLink');

			if($this.hasClass('activeImageLink')){
				$img.attr('src', $this.attr('href'));
				temp.imgArr = $('#temp_files').find('.activeImageLink');

				//don't bother with this is it's not visible!
				if(!$bool){
					$tempViewer.empty().append($img);
				}
				
				$editBtn.prop('disabled', false);
				$deleteBtn.prop('disabled', false);
			}else{
				temp.imgArr = $('#temp_files').find('.activeImageLink');
				if(!temp.imgArr.length){
					if(!$bool){
						$tempViewer.empty().append(temp.tempContent);
					}

					$editBtn.prop('disabled', true);
					$deleteBtn.prop('disabled', true);
				}
			}
		});
	},

	moveFiles : function(btn){
		var $btn = $(btn),
		$form = $btn.parents('ul').siblings('form.'+$btn.attr('class')),
		$imgArr = [],
		confirmDel = false,
		move = false;

		if($btn.hasClass('delete_temp_files')){
			confirmDel = confirm('Are you sure you want to delete these files?');
		}else if($btn.hasClass('move_temp_files')){
			move = true;
		}

		if(move || confirmDel){
			if(form_actions.checked_images !== null && form_actions.checked_images.length > 0){
				$.each(form_actions.checked_images, function(){
					$imgArr.push($(this).attr('data-id'));
				});
				$form.find('input[name="ID"]').val($imgArr);
				$form.find('input[name="imgArr[]"]').val(null);
			}else{
				$.each(temp.imgArr, function(){
					$imgArr.push($(this).attr('href'));
				});

				$form.find('input[name="imgArr[]"]').val($imgArr);
				$form.find('input[name="ID"]').val(null);
			}
		}

		if(confirmDel){
			$form.trigger('submit');
		}
	}
};

var search = {

	autoComplete : null,
	tagDelete : null,
	tagValue : null,
	tagJSON : null,

	tags : function(searchBar, bool){
		//need a slight delay or won't funtion currectly when tabbing through inputs
		if(bool){
			setTimeout(function(){
				search.autoComplete = searchBar;
			}, 300);
		}

		$.get(utils.url+'login/getTags', function(resp){
			search.tagJSON = $.parseJSON(resp);

			$(searchBar).autocomplete({
				lookup : search.tagJSON || null,
				onSelect : function(suggestion){
					var $this = $(this);

					if($this.hasClass('new_tag')){
						var e = jQuery.Event('keyup');
						//trigger 'comma' with keyup
						e.which = 188;
						e.keyCode = 188;
						setTimeout(function(){
							if($this.val() == suggestion.value){
								$this.val($this.val()+',');
								$this.trigger(e);
								$this.focus();
							}
						}, 800);
					}
				}
			});
		});

	},

	submit_search : function(btn){
		var $btn = $(btn),
		$link = utils.url+'index/index/',
		$input = $btn.siblings('input'),
		$inputVal = $input.val(),
		$tag_id = null;

		$.each(search.tagJSON, function(i, item){
			if($inputVal == item.value){
				$tag_id = item.key;
			}
		});

		$tag_id = $tag_id === null ? $inputVal : $tag_id;

		$finalURL = $link + $tag_id;
		document.location.href = $finalURL;
		
	},

	removeAutoComplete : function(){
		//this fires onblur but we want to be able to select a suggestion with the mouse
		//without a slight delay the autocomplete instance is removed and the selection goes with it when trying to acomplish this hence the timeout
		setTimeout(function(){
			$(search.autoComplete).autocomplete('dispose');
		}, 200);
		
	},

	tagBrowser : function(cont){
		var $cont = $(cont);

		$cont.on('dblclick click', '.tag_edit', function(e){
			e.preventDefault();

			if(e.type != 'click'){
				var $this = $(this),
				$parent = $this.parent(),
				$value = $this.text();
				//set value for change comparrison
				search.tagValue = $value;

				$parent.addClass('activeEdit');
				$(this).replaceWith('<input onblur="search.submitEdit(this)" class="inline_edit" value="'+$value+'" autofocus type="text">');
				}
			
		});

		$cont.on('click', '.tag_delete', function(e){
			e.preventDefault();
			if(confirm('Are you sure?')){
				//no actual form so we declare the action here
				var $action = utils.url+'login/deleteTags',
				//grab the form id to be deleted
				$tag_id = $(this).parent().attr('data-id'),
				//create a new formData object
				$formData = new FormData();
				//append the ID under the POST name tag_id
				$formData.append('tag_id', $tag_id);
				//store parent for use in ajaxResult
				search.tagDelete = $(this).parent();
				//send the form using ajaxSend function
				form_actions.ajaxSend($action, $formData);
				}
		});
	},

	submitEdit : function(input){
		//grab the input
		var $input = $(input),
		//updated value
		$value = $input.val(),
		//id
		$tag_id = $input.parent().attr('data-id'),
		//construct new form data
		$formData = new FormData(),
		//form action
		$action = utils.url+'login/editTags';
		//set search field for later use
			search.tagDelete = $input;
		//check if any changes have been made
		if(search.tagValue == $value){
			search.tagDelete.parent().removeClass('activeEdit');
			search.tagDelete.replaceWith($('<a class="tag_edit" href="#">'+$value+'</a>'));
		}else{
			//append ID & value to form data
			$formData.append('tag_id', $tag_id);
			$formData.append('tag_title', $value);
			//ready! hit up the server
			form_actions.ajaxSend($action, $formData);
		}
		
	},

	searchMobile : function(btn){
		if(utils.mediaQuery.matches){
			var $btn = $(btn);
			$btn.toggleClass('activeSearch');
			if($btn.hasClass('activeSearch')){
				$btn.siblings().fadeIn();
			}else{
				$btn.siblings().fadeOut();
			}
		}
	},

	sortList : function(btn, container, sortType){
		//remove active class from all btns
		var btns = document.getElementsByClassName('sortOrder');
		for(var i = 0; i < btns.length; i++){
			btns.item(i).classList.remove('active');
		}
		//add active class to clicked btn
		btn.classList.add('active');
		//remove active stlyes from btns and then add to btn that has been clicked
		var domObj = document.getElementsByClassName(container)[0];
		var domObjChildren = domObj.children;
		var itemArr = [], result;
		for(var i in domObjChildren){
			itemArr.push(domObjChildren[i]);
		}
		var firstItem = itemArr.shift(); //unshift after sort
		var secondItem = itemArr.shift(); //unshift after 
		var thirdItem = itemArr.shift();
		
		switch(sortType){
			case 'nameASC' : 
				result = search.mapAndSort(itemArr, 'name', true);
			break;

			case 'nameDSC' :
				result = search.mapAndSort(itemArr, 'name', false);
			break;

			case 'countASC' :
				result = search.mapAndSort(itemArr, 'count', true);
			break;

			case 'countDSC' :
				result = search.mapAndSort(itemArr, 'count', false);
			break;

			case 'rand' :
				result = search.mapAndSort(itemArr, 'rand', false);
			break;

			default : 
				result = itemArr;
			break;
		}
		result.unshift(firstItem, secondItem, thirdItem); //place the first two elements back into the array
		
		//adding elements back to the DOM eliminates all white space which affects layout when using display : inline-block so we add a class to fix this
		domObj.classList.add('nonWhiteSpaceMargin');
		result.forEach(function(elem){
			domObj.appendChild(elem);
		});
	},

	mapAndSort : function(itemArr, context, order){
		//create a new array with the original index and the aspect of the element that we want to compare
		var mapped = itemArr.map(function(elem, index){
			if(context === 'name' && elem.children !== undefined){
				return {
					index : index,
					name : elem.children[1].children[0].innerText
				};
			}else if(elem.children !== undefined){
				return {
					index : index,
					count : elem.children[0].innerText
				};
			}
		});
		//sort the new array based on the context, name or count
		switch (context){
			case 'name' : 
				mapped.sort(function(a,b){
					if(order){ //true is ASC, false DSC
						return a.name > b.name ? 1 : -1;
					}
					return b.name > a.name ? 1 : -1;
				});
			break;

			case 'count' : 
				mapped.sort(function(a,b){
					if(order){ //true is ASC, false DSC
						return a.count - b.count;
					}
					return b.count - a.count;
				});
			break;

			case 'rand' : 
				for(var i = mapped.length - 1; i > 0; i--){
					var randNum = Math.floor(Math.random() * (i + 1));
					var temp = mapped[i];
					mapped[i] = mapped[randNum];
					mapped[randNum] = temp;
				}
			break;

			default : 
				//nothing doing
			break;
		}
		//populate the final array with sorted data and the original array 
		var result = [];
		for(var i = 0; i < mapped.length; i++){
			if(typeof mapped[i] == 'object'){
				result.push(itemArr[mapped[i].index]);	
			}
		}
		return result;
	}
};

var notes = {
	rawText : null,
	content : null,
	textarea : null,
	noteColour : null,

	noteColourDetect : function($cont){
		//grab the css style relating to note_colours
		var $mk_colour = $cont[0].className.split(/\s+/).filter(function(item, index, arr){
			if(item.indexOf('note_') > -1){
				return item;
			}
		});
		//check that note colour class is in the allowed list, or exists at all
		var $colour_arr = ['note_white', 'note_black', 'note_orange', 'note_ruby', 'note_blue', 'note_green'];
		var $colourTest = $colour_arr.some(function(item, index, arr){
			return $mk_colour == item;
		});
		//set the default to 'note_black' if test fails
		if(!$colourTest){
			$mk_colour = "note_black";
		}

		return String($mk_colour);
	},

	noteEdit : function(btn){
		var $btn = $(btn),
		$id = $btn.attr('data-id'),
		$tag_cont = $btn.parents('.notes_md'),
		$mk_content = $tag_cont.find('.note_parsed_content'),
		$mk_entry = $tag_cont.find('.edit_note_ta');
		$postURL = $btn.attr('data-posturl');

		if($btn.hasClass('activeBtn')){
			$.get(utils.url+'images/getNoteContent/'+$id, function(resp){
				$mk_entry.val(resp).show();
				$mk_content.hide();
				notes.rawText = resp;
				notes.noteColour = notes.noteColourDetect($tag_cont);
			});	
		}else{
			$mk_colour_new = notes.noteColourDetect($tag_cont);
			if($mk_entry.val() === ''){
				$mk_entry.hide();
				$mk_content.show();
			}else if($mk_entry.val() == notes.rawText && notes.noteColour.toString() == $mk_colour_new.toString()){
				$mk_entry.val('').hide();
				$mk_content.show();
			}else{
				//send to server
				notes.content = $mk_content;
				notes.textarea = $mk_entry;
				$formData = new FormData();
				$formData.append('note_id', $id);
				$formData.append('note_content', $mk_entry.val());
				$formData.append('note_colour', $mk_colour_new);
				form_actions.ajaxSend($postURL, $formData);
			}
		}	
	},

	noteSyntax : function(){
		var $form = $(document.forms['add_note']),
		$syntaxCont = $('#note_syntax');
		$form.on('click', '#note_syntax_toggle', function(e){
			var $this = $(this);
			e.preventDefault();
			$.get($this.attr('href'), function(resp){
				if($syntaxCont.find('.guide').length){
					$syntaxCont.find('.guide').remove();
					$syntaxCont.hide();
					$this.text('(show note syntax)');
					document.getElementById('bookmark_form').style.left = null;

				}else{
					$syntaxCont.append(resp);
					$syntaxCont.show();
					$this.text('(hide note syntax)');
					document.getElementById('bookmark_form').style.left = '50%';
				}
			});
		});
	},

	noteColourSelect : function(){
		//grab all links that change note colour
		var $note_colours = document.getElementsByClassName('change_colour');
		//cannot add addEventListener directly to a htmlCollection/nodelist so we run a foreach on an empty array that is then populated with the elements we found
		[].forEach.call($note_colours, function(item, index, arr){
			//add click handler
			item.addEventListener('click', function(e){
				e.preventDefault();
				var $class = this.getAttribute('data-colour');
				//apply to parent li or modal form container
				var $currentElem = this.parentNode;
				while(!$currentElem.classList.contains('notes_md') && !$currentElem.classList.contains('persistant')){
					//traverse the DOM upwards until we find the li with the correct class name
					$currentElem = $currentElem.parentNode;
				}
				//we don't know what the class name is that we are removing so let's grab anything that starts with 'note_'
				var $currentClass;
				$currentElem.className.split(/\s+/).filter(function(item, index, arr){
					if(item.indexOf('note_') > -1){
						$currentClass = arr[index];
					}
				});
				$currentElem.classList.remove($currentClass);
				$currentElem.classList.add($class);
			});
		});
	} 
};

var ajaxResponses = {

	editComplete : function(data){
		utils.simpleSuccess();
		$input = $('.activeInput');
		if(data.title){
			$titleCont = $input.siblings('.editable');
			$header = $titleCont.children();
			if($header.length > 0){
				$header.text(data.title);
			}else{
				$titleCont.text(data.title);
			}
			$input.hide().removeClass('activeInput');
			$titleCont.show();
		}else{
			$titleCont = $input.siblings('.editable');
			$titleCont.children().attr('href', data.url);
			$input.hide().removeClass('activeInput');
		}
	},

	tagDeleted : function(){
		utils.simpleSuccess();
		search.tagDelete.addClass('removeElem');
		setTimeout(function(){
			search.tagDelete.remove();
		}, 600);
	},

	tagEdited : function(){
		utils.successRemove();
		search.tagDelete.parent().removeClass('activeEdit');
		var $value = search.tagDelete.val();
		search.tagDelete.replaceWith($('<a class="tag_edit" href="#">'+$value+'</a>'));
	},

	tagNotAdded : function(){
		utils.error('Tags were not changed');
		setTimeout(function(){
			form_actions.activeForm.find('.cancel').trigger('click');
			form_actions.activeForm = null;
		}, 800);
	},

	bookDeleted : function(){
		utils.simpleSuccess();
		var $multi = false;
		if(form_actions.checked_images !== null && !form_actions.deleteMultiBook){
			$multi = true;
			$.each(form_actions.checked_images, function(){
				$(this).parents('li').addClass('removeElem');
			});
			form_actions.deleteMultiBook = false;
		}else{
			var $elem = form_actions.activeForm.parent('li');
			$elem.addClass('removeElem');
			form_actions.deleteMultiBook = false;
		}
		
		setTimeout(function(){
			if($multi){
				$.each(form_actions.checked_images, function(){
					$(this).parents('li').remove();
				});
				form_actions.checked_images = null;
			}else{
				$elem.remove();
			}
			form_actions.activeForm = null;
		}, 800);
	},

	bookmarkAdded : function(){
		//reload if we're on the bookmark page
		if(form_actions.deleteMultiBook){
			ajaxResponses.reload();
		}else{
			//otherwise
			utils.simpleSuccess();
			//trigger the cancel button to hide and reset form
			document.forms['add_bookmark'].getElementsByClassName('cancel')[0].click();
		}
	},

	noteUpdated : function(data){
		utils.simpleSuccess();
		notes.content.html(data.content).show();
		notes.content = null;
		notes.textarea.hide().val('');
		notes.rawText = '';
	},

	noteNotUpdated : function(){
		utils.simpleSuccess();
		notes.content.show();
		notes.content = null;
		notes.textarea.hide().val('');
		notes.rawText = null;
	},

	tempDeleted : function(){
		utils.simpleSuccess();
		//add remove animation in first loop
		$.each(temp.imgArr, function(){
			$(this).parent('li').addClass('removeElem');
		});

		//run second loop on delay to remove from DOM
		setTimeout(function(){
			$.each(temp.imgArr, function(){
				$(this).parent('li').remove();
			});

			//disable buttons, empty tempViewer and crucially empty the temp array otherwise we get errors
			$('.move_temp_files').prop('disabled', true);
			$('.delete_temp_files').prop('disabled', true);
			$('#temp_files_viewer').empty().append(temp.tempContent);

			temp.imgArr = null;

			form_actions.activeForm.find('.cancel').trigger('click');
			form_actions.activeForm = null;
		}, 800);
	},

	imageAdded : function(data){
		utils.success();
		setTimeout(function(){
			utils.successRemove();
			form_actions.activeForm.find('.cancel').trigger('click');
			form_actions.activeForm = null;
		}, 800);

		var $location = document.location.href;

		if($('.images, section').length > 0){
			setTimeout(function(){
				document.location.href = $location;
			}, 1000);
		}else if(data.refresh){
			setTimeout(function(){
				document.location.href = $location;
			}, 1000);
		}
	},

	reload : function(){
		utils.success();
		setTimeout(function(){
			document.location.href = document.location.href;
		}, 800);
	},

	defaultFormResp : function(){
		utils.simpleSuccess();
		setTimeout(function(){
			form_actions.activeForm.siblings('.button_container').find('.activeBtn').trigger('click');
			form_actions.activeForm.siblings('.description').find('.activeBtn').trigger('click');
			form_actions.activeForm = null;
		}, 800);
	},
};

/*
 * jQuery.liveFilter
 *
 * Copyright (c) 2009 Mike Merritt
 *
 * Forked by Lim Chee Aun (cheeaun.com)
 * 
 */
 
(function($){
	$.fn.liveFilter = function(inputEl, filterEl, options){
		var defaults = {
			filterChildSelector: null,
			filter: function(el, val){
				return $(el).text().toUpperCase().indexOf(val.toUpperCase()) >= 0;
			},
			before: function(){},
			after: function(){}
		};
		var options = $.extend(defaults, options);
		
		var el = $(this).find(filterEl);
		if (options.filterChildSelector) el = el.find(options.filterChildSelector);

		var filter = options.filter;
		$(inputEl).keyup(function(){
			var val = $(this).val();
			var contains = el.filter(function(){
				return filter(this, val);
			});
			var containsNot = el.not(contains);
			if (options.filterChildSelector){
				contains = contains.parents(filterEl);
				containsNot = containsNot.parents(filterEl).hide();
			}
			
			options.before.call(this, contains, containsNot);
			
			contains.show();
			containsNot.hide();
			
			if (val === '') {
				contains.show();
				containsNot.show();
			}
			
			options.after.call(this, contains, containsNot);
		});
	}
})(jQuery);
