<?php

class Rss extends Controller{
	function __construct(){
		parent::__construct();
		Auth::handleLogin();
	}

	public function index($tag = NULL){
		$tagArr = $tag !== NULL && preg_match('~^[0-9]+(,[0-9]+)*$~i', $tag)? explode(',', $tag) : $tag;
		$rss_model = $this->loadModel('rss');

    	$this->view->tags = is_array($tagArr) ? $rss_model->getTags('f', $tagArr) : $this->view->tags = $rss_model->getTags('f');

    	$this->view->feeds = $rss_model->getMedia('f', NULL, 50, $tagArr);
    	//required for searches
		if($tagArr !== NULL && $tagArr !== 'tl' ){
			$this->view->search = str_replace('_', ' ', $tagArr);
		}
		//
		$this->view->tagCounts = $rss_model->properTagCounts();
		$this->view->render('rss/index');
	}

	public function loadMore($page, $tag = NULL){
		$tagArr = $tag !== NULL && preg_match('~^[0-9]+(,[0-9]+)*$~i', $tag)? explode(',', $tag) : $tag;
		$rss_model = $this->loadModel('rss');
		$this->view->page = $page;
    	$this->view->feeds = $rss_model->getMedia('f', $page, 50, $tagArr);
    	//required for searches
		if($tagArr !== NULL && $tagArr !== 'tl' ){
			$this->view->search = implode(',', $tagArr);
		}
		//
		$this->view->render('rss/loadmore', true);
	}

	public function editFeeds(){
		$rss_model = $this->loadModel('rss');
    	$this->view->urls = $rss_model->getMedia('fu', NULL);
		$this->view->render('rss/editFeeds');
	}

	public function add(){
		if($this->edit_able){
			if(isset($_POST['feed_url']) && !empty($_POST['feed_url']) && isset($_POST['tags'])){
				//convert tags into an array
				$tags = !empty($_POST['tags']) ? explode(',', $_POST['tags']) : NULL;
				$rss_model = $this->loadModel('rss');

				if($rss_model->add($_POST['feed_url'], $tags)){
					echo json_encode(array('msg' => 'Feed_Added', 'error' => false));
				}else{
					echo json_encode(array('msg' => 'Feed_Not_Added', 'error' => true));
				}

			}else{
				echo json_encode(array('msg' => 'Feed_Not_Added', 'error' => true));
			}
		}else{
			echo json_encode(array('msg' => 'Disabled in demo', 'error' => true));
		}
	}

	public function edit(){
		if($this->edit_able){
			//js function appends form data with book_* prefix and we reuse that function(submitEdit) here
			if(isset($_POST['book_url']) && !empty($_POST['book_url']) && isset($_POST['book_id']) && is_numeric($_POST['book_id'])){

				$rss_model = $this->loadModel('rss');

				if($rss_model->edit($_POST['book_url'], $_POST['book_id']) > 0){
					echo json_encode(array('error' => false, 'msg' => 'URL_Updated', 'title' => $_POST['book_url']));
				}else{
					echo json_encode(array('error' => true, 'msg' => 'URL_NOT_Updated'));
				}		 
			}else{
				echo json_encode(array('error' => true, 'msg' => 'URL_NOT_Updated'));
			}
		}else{
			echo json_encode(array('error' => true, 'msg' => 'Disabled in demo'));
		}
	}

	public function deleteFeed(){
		if($this->edit_able){
			if(isset($_POST['feed_id']) && is_numeric($_POST['feed_id'])){
				$rss_model = $this->loadModel('rss');
				if($rss_model->delete($_POST['feed_id'])){
					echo json_encode(array('msg'=>'feed_deleted', 'error'=>false));
				}else{
					echo json_encode(array('msg'=>'Feed not deleted', 'error'=>true));
				}
			}else{
				echo json_encode(array('msg'=>'There was an error deleting the feed', 'error'=>true));
			}
		}else{
			echo json_encode(array('msg'=>'feed_deleted', 'error'=>false));
		}
	}

	public function setMediaType(){
		return 'f';
	}	

	public function getContent($id, $mark = false){
		$rss_model = $this->loadModel('rss');
		$this->view->content = $rss_model->getContent($id);
		if($mark){
			$this->markAsRead(0, $id, $rss_model);
			$this->view->render('rss/content', true);
		}else{
			$this->view->render('rss/content', true);
		}
	}

	public function refreshFeeds(){
		if($this->edit_able){
			$rss_model = $this->loadModel('rss');
			$result = $rss_model->refreshFeeds();
			if($result){
				echo json_encode(array('msg' => 'feeds_updated', 'error' => false, 'refresh' => true));
			}else{
				echo json_encode(array('msg' => 'feeds_not_updated', 'error' => true, 'refresh' => false));
			}
		}else{
			echo json_encode(array('msg' => 'Disabled in demo', 'error' => true, 'refresh' => false));
		}
	}

	public function markAsRead($flag, $id = null, $model = null){
		if($this->edit_able){
			//flag : 0.single, 1.day, 7.week, 31.month, 100.all
			$rss_model = $model === null? $this->loadModel('rss') : $model;
			if($id !== null && $flag == 0){
				$result = $rss_model->markSingleRead($id);
			}else{
				if($flag == 1){
					$result = $rss_model->markDayRead();
				}else if($flag == 7){
					$result = $rss_model->markWeekRead();
				}else if($flag == 31){
					$result = $rss_model->markMonthRead();
				}else if($flag == 100){
					$result = $rss_model->markAllRead();
				}
			}

			if($result && $id !== null){
				return true;
			}else if($result){
				echo json_encode(array('msg' => 'Marked', 'error' => false));
			}else{
				echo json_encode(array('msg' => 'Could not mark feeds as read', 'error' => true));
			}
		}else{
			return true;
		}
	}
}