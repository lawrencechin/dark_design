<?php

class BookmarkModel extends MasterModel{
	/**
     * Constructor, expects a Database connection
     * @param Database $db The Database object
     */
	public function __construct(Database $db){
		parent::__construct($db);
	}

	public function create($book_title, $book_url, $tags){
		$sql = "INSERT INTO Bookmarks(user_id, Title, URL) VALUES(:user_id, :Title, :URL)";

		$dataArr = array(
			'user_id' => $_SESSION['user_id'],
			'Title' => strip_tags($book_title),
			'URL' => strip_tags($book_url)
			);
		$result = $this->commitDb($sql, $dataArr, false, true);

		if($result){ //result should be last inserted ID if successful
			//bookmark added - now lets add tags if set
			if($tags !== NULL){
				$this->tags($tags, 'b', $result);
				return true;
			}else{
				//done - no tagging
				return true;
			}
		}

		$this->write_feedback(FEEDBACK_BOOK_CREATION_FAILED);
		return false;
	}

	public function edit($book_id = NULL, $book_title = NULL, $book_url = NULL){
		$edit_url = false;
		$edit_title = false;
		//update url	
		if($book_title === NULL && $book_url !== NULL){
			$edit_url = true;
		//update title	
		}else if($book_url === NULL && $book_title !== NULL){
			$edit_title = true;
		}
		//generate sql statements based on booleans
		if($edit_title){
			$sql = "UPDATE Bookmarks SET Title = :Title WHERE user_id = :user_id AND ID = :ID";
			$dataArr = array(
				'user_id' => $_SESSION['user_id'],
				'ID' => $book_id,
				'Title' => $book_title);
		}else if($edit_url){
			$sql = "UPDATE Bookmarks SET URL = :URL WHERE user_id = :user_id AND ID = :ID";
			$dataArr = array(
				'user_id' => $_SESSION['user_id'],
				'ID' => $book_id,
				'URL' => $book_url);
		}else{
			//no criteria matched, end the function
			return 0;
		}

		return $this->commitDb($sql, $dataArr, false);
	}

	public function delete($book_id){
		//create array from POST data - this will either be a single ID or a comma seperated string
		$bookArr = explode(',' , $book_id);
		$errArr = [];
		//loop through array and delete
		foreach($bookArr as $id){
			$sql = "DELETE FROM Bookmarks WHERE user_id = :user_id AND ID = :ID";
			$dataArr = array(
				'user_id' => $_SESSION['user_id'],
				'ID' => $id
				);
			
			if($this->commitDb($sql, $dataArr, false) > 0){
				//bookmark deleted, now let's remove tag associations
				$this->removeTag(NULL, $id, 'b');
			}else{
				//in the event deletion failed
				array_push($errArr, $id);
			}
		}

		if(count($errArr) > 0){
			$this->write_feedback(FEEDBACK_BOOK_DELETION_FAILED);
			return [false];
		}
		//return different response if multiple ids detected
		if(count($bookArr) > 1){
			return [true, true];
		}

		return [true, false];
	}

	public function exportBookmarks($bookmarks, $dir){
		if(isset($bookmarks) && count($bookmarks) > 1){
			//create a netscape bookmark file - can be imported into browser bookmarks
			$header = "<!DOCTYPE NETSCAPE-Bookmark-file-1>\n<HTML>\n<META HTTP-EQUIV=\"Content-Type\" CONTENT=\"text/html; charset=UTF-8\">\n<Title>Bookmarks</Title>\n<H1>Bookmarks</H1>\n<DT><H3 FOLDED>Dark Design Favs</H3>\n<DL><p>\n";
			$contentStr = '';
			$footer = "</DL><p>\n</HTML>";
			$filename = 'bookmarks_export.html';

			foreach($bookmarks as $books){
				if(!isset($books->count)){
					$contentStr .= "<DT><A HREF=\"".$books->URL."\">".$books->Title."</A>\n";
				}
			}

            $handle = fopen($dir . $filename, 'w');
            fwrite($handle, $header . $contentStr . $footer);
            fclose($handle);
			return array('success' => true, 'path' => $dir, 'filename' => $filename);
		}else{
			return array('success' => false);
		}
	}
}